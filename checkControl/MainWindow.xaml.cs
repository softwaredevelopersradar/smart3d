﻿using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace checkControl
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            List<TableUAVRes> lTablesUAVRes = new List<TableUAVRes>();

            TableUAVRes tableUAVRes = new TableUAVRes()
            { Id = 0 };

            lTablesUAVRes.Add(tableUAVRes);

            tableUAVRes = new TableUAVRes()
            { Id = 1 };

            lTablesUAVRes.Add(tableUAVRes);

            List<TableUAVTrajectory> lTableUAVTrajectory = new List<TableUAVTrajectory>();

            TableUAVTrajectory tableUAVTrajectory = new TableUAVTrajectory()
            {
                TableUAVResId = 0,
                Coordinates = new Coord()
                {
                    Altitude = 10,
                    Latitude = 20,
                    Longitude = 30
                }
            };

            lTableUAVTrajectory.Add(tableUAVTrajectory);

            tableUAVTrajectory = new TableUAVTrajectory()
            {
                TableUAVResId = 0,
                Num = 1,
                Coordinates = new Coord()
                {
                    Altitude = 15,
                    Latitude = 25,
                    Longitude = 35
                }
            };

            lTableUAVTrajectory.Add(tableUAVTrajectory);

            tableUAVTrajectory = new TableUAVTrajectory()
            {
                TableUAVResId = 1,
                Coordinates = new Coord()
                {
                    Altitude = 40,
                    Latitude = 50,
                    Longitude = 60
                }
            };

            lTableUAVTrajectory.Add(tableUAVTrajectory);


            s3D.Plot(lTablesUAVRes, lTableUAVTrajectory);
        }
    }
}
