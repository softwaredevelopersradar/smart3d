﻿using Arction.Wpf.SemibindableCharting;
using Arction.Wpf.SemibindableCharting.Annotations;
using Arction.Wpf.SemibindableCharting.Series3D;
using Arction.Wpf.SemibindableCharting.Views.View3D;
using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Color = System.Windows.Media.Color;

namespace smartControl3D
{
    public partial class Control3D
    {
        Dictionary<int, int> ColorIDPicker = new Dictionary<int, int>();
        Dictionary<string, int> ColorIDPickerAero = new Dictionary<string, int>();

        private void ColorWork(List<int> ListID)
        {
            //Если пустой то заполняем
            if (ColorIDPicker.Count == 0)
            {
                for (int i = 0; i < ListID.Count(); i++)
                {
                    ColorIDPicker.Add(ListID[i], i);
                }
            }
            //А если не пустой, то как работнём
            else
            {
                List<int> ContainsList = new List<int>();
                List<int> NotContainsList = new List<int>();

                for (int i = 0; i < ListID.Count(); i++)
                {
                    if (ColorIDPicker.ContainsKey(ListID[i]))
                    {
                        ContainsList.Add(ListID[i]);
                    }
                    else
                    {
                        NotContainsList.Add(ListID[i]);
                    }
                }

                if (NotContainsList.Count != 0)
                {
                    for (int i = 0; i < NotContainsList.Count(); i++)
                    {
                        if (ColorIDPicker.Count < UAVDronesCount)
                        {
                            ColorIDPicker.Add(NotContainsList[i], ColorIDPicker.Count());
                        }
                        else
                        {
                            for (int j = 0; j < ColorIDPicker.Count(); j++)
                            {
                                if (!ContainsList.Any(x => x == ColorIDPicker.ElementAt(j).Key))
                                {
                                    int key = ColorIDPicker.ElementAt(j).Key;
                                    int value = ColorIDPicker.ElementAt(j).Value;

                                    ColorIDPicker.Remove(key);
                                    ColorIDPicker.Add(NotContainsList[i], value);
                                }
                            }
                        }
                    }
                }

            }

        }

        private void ColorWorkAero(List<string> ListSN)
        {
            //Если пустой то заполняем
            if (ColorIDPickerAero.Count == 0)
            {
                for (int i = 0; i < ListSN.Count(); i++)
                {
                    ColorIDPickerAero.Add(ListSN[i], i);
                }
            }
            //А если не пустой, то как работнём
            else
            {
                List<string> ContainsList = new List<string>();
                List<string> NotContainsList = new List<string>();

                for (int i = 0; i < ListSN.Count(); i++)
                {
                    if (ColorIDPickerAero.ContainsKey(ListSN[i]))
                    {
                        ContainsList.Add(ListSN[i]);
                    }
                    else
                    {
                        NotContainsList.Add(ListSN[i]);
                    }
                }

                if (NotContainsList.Count != 0)
                {
                    for (int i = 0; i < NotContainsList.Count(); i++)
                    {
                        if (ColorIDPickerAero.Count < UAVDronesCount)
                        {
                            ColorIDPickerAero.Add(NotContainsList[i], ColorIDPickerAero.Count());
                        }
                        else
                        {
                            for (int j = 0; j < ColorIDPickerAero.Count(); j++)
                            {
                                if (!ContainsList.Any(x => x == ColorIDPickerAero.ElementAt(j).Key))
                                {
                                    string key = ColorIDPickerAero.ElementAt(j).Key;
                                    int value = ColorIDPickerAero.ElementAt(j).Value;

                                    ColorIDPickerAero.Remove(key);
                                    ColorIDPickerAero.Add(NotContainsList[i], value);
                                }
                            }
                        }
                    }
                }

            }

        }

        private void ColorPallete()
        {
            List<Color> Pallete = new List<Color>()
            {
            Color.FromArgb(255, 0, 0, 254),
            Color.FromArgb(255, 0, 210, 1),
            Color.FromArgb(255, 254, 0, 0),
            Color.FromArgb(255, 255, 115, 0),
            Color.FromArgb(255, 101, 0, 128),
            Color.FromArgb(255, 111, 112, 254),
            Color.FromArgb(255, 0, 96, 0),
            Color.FromArgb(255, 254, 124, 124),
            Color.FromArgb(255, 145, 66, 0),
            Color.FromArgb(255, 165, 112, 180)
            };

            Color color0 = Color.FromArgb(255, 0, 0, 254);
            Color color1 = Color.FromArgb(255, 0, 210, 1);
            Color color2 = Color.FromArgb(255, 254, 0, 0);
            Color color3 = Color.FromArgb(255, 255, 115, 0);
            Color color4 = Color.FromArgb(255, 101, 0, 128);
            Color color5 = Color.FromArgb(255, 111, 112, 254);
            Color color6 = Color.FromArgb(255, 0, 96, 0);
            Color color7 = Color.FromArgb(255, 254, 124, 124);
            Color color8 = Color.FromArgb(255, 145, 66, 0);
            Color color9 = Color.FromArgb(255, 165, 112, 180);
        }
        private Color ColorPallete0(int index)
        {
            List<Color> Pallete = new List<Color>()
            {
            Color.FromArgb(255, 0, 0, 254),
            Color.FromArgb(255, 0, 210, 1),
            Color.FromArgb(255, 254, 0, 0),
            Color.FromArgb(255, 255, 115, 0),
            Color.FromArgb(255, 101, 0, 128),
            Color.FromArgb(255, 111, 112, 254),
            Color.FromArgb(255, 0, 96, 0),
            Color.FromArgb(255, 254, 124, 124),
            Color.FromArgb(255, 145, 66, 0),
            Color.FromArgb(255, 165, 112, 180)
            };

            return Pallete[index];
        }

        private Color ColorPallete(int index)
        {
            List<Color> Pallete = new List<Color>()
            {
            Color.FromArgb(255, 254, 0, 0),
            Color.FromArgb(255, 0, 0, 254),
            Color.FromArgb(255, 0, 210, 1),
            Color.FromArgb(255, 255, 115, 0),
            Color.FromArgb(255, 101, 0, 128),
            Color.FromArgb(255, 111, 112, 254),
            Color.FromArgb(255, 0, 96, 0),
            Color.FromArgb(255, 254, 124, 124),
            Color.FromArgb(255, 145, 66, 0),
            Color.FromArgb(255, 165, 112, 180)
            };

            return Pallete[index];
        }

        private Color ColorPalleteGrey80(int index)
        {
            List<Color> Pallete = new List<Color>()
            {
            Color.FromArgb(255, 174, 81, 81),
            Color.FromArgb(255, 81, 81, 174),
            Color.FromArgb(255, 67, 143, 67),
            Color.FromArgb(255, 174, 123, 81),
            Color.FromArgb(255, 78, 40, 87),
            Color.FromArgb(255, 156, 156, 209),
            Color.FromArgb(255, 28, 68, 28),
            Color.FromArgb(255, 214, 165, 165),
            Color.FromArgb(255, 99, 70, 46),
            Color.FromArgb(255, 145, 145, 145)
            };

            return Pallete[index];
        }

    }
}
