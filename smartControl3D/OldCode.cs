﻿using Arction.Wpf.SemibindableCharting;
using Arction.Wpf.SemibindableCharting.Annotations;
using Arction.Wpf.SemibindableCharting.Series3D;
using Arction.Wpf.SemibindableCharting.Views.View3D;
using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Color = System.Windows.Media.Color;

namespace smartControl3D
{
    public partial class Control3D
    {
        List<TableUAVRes> lTablesUAVRes = new List<TableUAVRes>();

        List<TableUAVTrajectory> lTableUAVTrajectory = new List<TableUAVTrajectory>();

        public void Plot(List<TableUAVRes> TablesUAVResList, List<TableUAVTrajectory> TableUAVTrajectoryList)
        {
            if (_chart.View3D.MeshModels.Count > 1)
            {
                _chart.View3D.MeshModels.RemoveRange(1, _chart.View3D.MeshModels.Count);
            }

            //Очистить траектории
            _chart.View3D.PointLineSeries3D.Clear();

            //Получить ListId
            List<int> ListID = TablesUAVResList.Select(x => x.Id).ToList();

            for (int i = 0; i < ListID.Count(); i++)
            {
                //Получить координаты дрона из траекторий

                //Получить ID для работы
                int currentID = ListID[i];

                //Получаем траекторию по ID
                List<TableUAVTrajectory> selectCurrentTraj = TableUAVTrajectoryList.Where(x => x.TableUAVResId == currentID).ToList();

                if (selectCurrentTraj.Count > 0)
                {
                    int MaxNum = selectCurrentTraj.Max(x => x.Num);

                    //Получаем крайние координаты
                    Coord lastCoords = selectCurrentTraj.Where(x => x.Num == MaxNum).Select(x => x.Coordinates).FirstOrDefault();

                    //Добавить дрон
                    AddDrone((byte)TablesUAVResList[i].Type, lastCoords);

                    //Сделать траектории
                    PointLineSeries3D newDataSeries = MakeTraj();
                    PointLineSeries3D newDataSeries2 = MakeTraj2();

                    //Получить все координаты
                    List<Coord> Coords = selectCurrentTraj.Select(x => x.Coordinates).ToList();

                    //Заполнить траектории
                    newDataSeries.Points = Points3DFromCoords(Coords);
                    newDataSeries2.Points = Points3DFromCoords2(Coords);

                    //Добавить траеториии на 3D
                    _chart.View3D.PointLineSeries3D.Add(newDataSeries);
                    _chart.View3D.PointLineSeries3D.Add(newDataSeries2);
                }
            }

            SeriesPoint3D[] Points3DFromCoords(List<Coord> coords)
            {
                List<SeriesPoint3D> lseriesPoint3Ds = new List<SeriesPoint3D>();

                for (int i = 0; i < coords.Count; i++)
                {
                    lseriesPoint3Ds.Add(new SeriesPoint3D(coords[i].Latitude, coords[i].Altitude, coords[i].Longitude));
                }
                return lseriesPoint3Ds.ToArray();
            }
            SeriesPoint3D[] Points3DFromCoords2(List<Coord> coords)
            {
                List<SeriesPoint3D> lseries2Point3Ds = new List<SeriesPoint3D>();

                for (int i = 0; i < coords.Count; i++)
                {
                    lseries2Point3Ds.Add(new SeriesPoint3D(coords[i].Latitude, coords[i].Altitude, coords[i].Longitude));
                    lseries2Point3Ds.Add(new SeriesPoint3D(coords[i].Latitude, 0, coords[i].Longitude));
                    lseries2Point3Ds.Add(new SeriesPoint3D(coords[i].Latitude, coords[i].Altitude, coords[i].Longitude));
                }

                return lseries2Point3Ds.ToArray();
            }

            PointLineSeries3D MakeTraj()
            {
                PointLineSeries3D newDataSeries = new PointLineSeries3D()
                {
                    XAxisBinding = Axis3DBinding.Primary,
                    YAxisBinding = Axis3DBinding.Primary,
                    ZAxisBinding = Axis3DBinding.Primary,
                    MouseInteraction = false,
                    MouseHighlight = MouseOverHighlight.None,
                };

                newDataSeries.PointsVisible = false;
                newDataSeries.LineVisible = true;
                newDataSeries.LineStyle.Width = 0.3f;
                newDataSeries.Title.Text = "Position";
                newDataSeries.MouseInteraction = false;
                newDataSeries.LineStyle.Color = System.Windows.Media.Color.FromRgb(255, 0, 0);

                return newDataSeries;
            }
            PointLineSeries3D MakeTraj2()
            {
                PointLineSeries3D newDataSeries2 = new PointLineSeries3D()
                {
                    XAxisBinding = Axis3DBinding.Primary,
                    YAxisBinding = Axis3DBinding.Primary,
                    ZAxisBinding = Axis3DBinding.Primary,
                    MouseInteraction = false,
                    MouseHighlight = MouseOverHighlight.None,
                };

                newDataSeries2.PointsVisible = false;
                newDataSeries2.LineVisible = true;
                newDataSeries2.LineStyle.Width = 0.1f;
                newDataSeries2.Title.Text = "Trajectory";
                newDataSeries2.MouseInteraction = false;
                newDataSeries2.LineStyle.Color = System.Windows.Media.Color.FromRgb(255, 255, 0);

                return newDataSeries2;
            }

            void AddDrone(byte Type, Coord coord)
            {
                //Загрузить нужную модель дрона
                string path = System.Reflection.Assembly.GetEntryAssembly().Location;
                var lastindex = path.LastIndexOf("\\");
                path = path.Substring(0, lastindex);
                path = path + "\\Resources\\Drones";

                string pathToFile = path + "\\Drone" + Type + ".obj";

                var newDroneModel = new MeshModel(_chart.View3D, Axis3DBinding.Primary, Axis3DBinding.Primary, Axis3DBinding.Primary);

                newDroneModel.LoadFromFile(pathToFile);

                newDroneModel.Position.X = (float)coord.Latitude;
                newDroneModel.Position.Y = (float)coord.Altitude;
                newDroneModel.Position.Z = (float)coord.Longitude;

                newDroneModel.MouseInteraction = false;

                newDroneModel.Rotation.SetValues(0, 0, 0);

                newDroneModel.Size.SetValues(0.02f, 0.02f, 0.02f);

                //Добавить дрон
                _chart.View3D.MeshModels.Add(newDroneModel);

                //Добавить аннотацию
                AddAnnotation();
                SetAnnotation(_chart.View3D.MeshModels[_chart.View3D.MeshModels.Count - 1], _chart.View3D.MeshModels.Count - 2);

                void SetAnnotation(MeshModel meshModel, int index)
                {
                    _chart.View3D.Annotations[index].TargetAxisValues.SetValues(meshModel.Position.X, meshModel.Position.Y, meshModel.Position.Z);
                    _chart.View3D.Annotations[index].Text = "DJI" + "\nX: " + meshModel.Position.X.ToString("F6") + "\nY: " + meshModel.Position.Y.ToString("F0") + "\nZ: " + meshModel.Position.Z.ToString("F6");
                }
            }
        }
    }
}
