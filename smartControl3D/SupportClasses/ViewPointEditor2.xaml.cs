﻿using System;
using System.Windows;
using System.Windows.Controls;

using Arction.Wpf.SemibindableCharting;
using Arction.Wpf.SemibindableCharting.Views.View3D;
using Arction.Wpf.SemibindableCharting.Views.ViewPie3D;

namespace smartControl3D
{
    /// <summary>
    /// Interaction logic for UserControlViewPointEditor.xaml.
    /// </summary>
    public partial class ViewPointEditor2 : UserControl
    {
        // Related chart.
        private LightningChartUltimate _chart;
        // Related view.
        private View3DBase m_view;

        private SelectionChangedEventHandler m_comboBoxProjectionValueChanged;
        private SelectionChangedEventHandler m_ComboBoxCameraValueChanged;
        private RoutedPropertyChangedEventHandler<Double> m_distanceSliderValueChanged;
        private RoutedPropertyChangedEventHandler<Double> m_horizontalSliderValueChanged;
        private RoutedPropertyChangedEventHandler<Double> m_sideSliderValueChanged;
        private RoutedPropertyChangedEventHandler<Double> m_verticalSliderValueChanged;


        /// <summary>
        /// Keeps track if the change of camera values originate from this side and 
        /// causes skipping of the UI update. this way we do not override the values 
        /// we're to send in to the camera.
        /// </summary>
        private bool m_bOwnChange = false;

        public ViewPointEditor2()
        {
            InitializeComponent();

            _chart = null;
            m_view = null;

            m_comboBoxProjectionValueChanged = new SelectionChangedEventHandler(comboBoxProjection_SelectionChanged);
            m_distanceSliderValueChanged = new RoutedPropertyChangedEventHandler<Double>(sliderDistance_ValueChanged);
            m_horizontalSliderValueChanged = new RoutedPropertyChangedEventHandler<Double>(sliderHorizontalRotation_ValueChanged);
            m_sideSliderValueChanged = new RoutedPropertyChangedEventHandler<Double>(sliderSideRotation_ValueChanged);
            m_verticalSliderValueChanged = new RoutedPropertyChangedEventHandler<Double>(sliderVerticalRotation_ValueChanged);
            m_ComboBoxCameraValueChanged = new SelectionChangedEventHandler(ComboBoxCamera_SelectionChanged);

            EnabledChangedEvents();
        }

        /// <summary>
        /// Chart.
        /// </summary>
        public LightningChartUltimate Chart
        {
            get
            {
                return _chart;
            }

            set
            {
                _chart = value;

                if (_chart != null)
                {
                    switch (_chart.ActiveView)
                    {
                        case ActiveView.View3D:
                            m_view = _chart.View3D;
                            if (m_view != null)
                                ((View3D)m_view).CameraViewChanged += new View3D.CameraViewChangedHandler(ViewPointEditor_CameraViewChanged);
                            break;
                        case ActiveView.ViewPie3D:
                            m_view = _chart.ViewPie3D;
                            if (m_view != null)
                                ((ViewPie3D)m_view).CameraViewChanged += new ViewPie3D.CameraViewChangedHandler(ViewPointEditor_CameraViewChanged);
                            break;
                        default:
                            m_view = null;
                            break;
                    }
                }

                SetControlValuesFromChart();
                SetViewPoint();
            }
        }

        private void DisableChangedEvents()
        {
            comboBoxProjection.SelectionChanged -= m_comboBoxProjectionValueChanged;
            sliderDistance.ValueChanged -= m_distanceSliderValueChanged;
            sliderHorizontalRotation.ValueChanged -= m_horizontalSliderValueChanged;
            sliderSideRotation.ValueChanged -= m_sideSliderValueChanged;
            sliderVerticalRotation.ValueChanged -= m_verticalSliderValueChanged;
            textBoxExDepth.TextChanged -= textBoxExDepth_TextChanged;
            textBoxExHeight.TextChanged -= textBoxExHeight_TextChanged;
            textBoxExWidth.TextChanged -= textBoxExWidth_TextChanged;
            tbVerticalRotation.TextChanged -= TbVerticalRotation_TextChanged;
            tbHorizontalRotation.TextChanged -= TbHorizontalRotation_TextChanged;
            tbSideRotation.TextChanged -= TbSideRotation_TextChanged;
            tbDistance.TextChanged -= TbDistance_TextChanged;
            comboBoxCamera.SelectionChanged -= m_ComboBoxCameraValueChanged;
        }

        private void EnabledChangedEvents()
        {
            comboBoxProjection.SelectionChanged += m_comboBoxProjectionValueChanged;
            sliderDistance.ValueChanged += m_distanceSliderValueChanged;
            sliderHorizontalRotation.ValueChanged += m_horizontalSliderValueChanged;
            sliderSideRotation.ValueChanged += m_sideSliderValueChanged;
            sliderVerticalRotation.ValueChanged += m_verticalSliderValueChanged;
            textBoxExDepth.TextChanged += textBoxExDepth_TextChanged;
            textBoxExHeight.TextChanged += textBoxExHeight_TextChanged;
            textBoxExWidth.TextChanged += textBoxExWidth_TextChanged;
            tbVerticalRotation.TextChanged += TbVerticalRotation_TextChanged;
            tbHorizontalRotation.TextChanged += TbHorizontalRotation_TextChanged;
            tbSideRotation.TextChanged += TbSideRotation_TextChanged;
            tbDistance.TextChanged += TbDistance_TextChanged;
            comboBoxCamera.SelectionChanged += m_ComboBoxCameraValueChanged;
        }

        /// <summary>
        /// Set control values from chart properties.
        /// </summary>
        private void SetControlValuesFromChart()
        {
            if (m_view != null && !m_bOwnChange && !double.IsNaN(m_view.Camera.RotationX) && !double.IsNaN(m_view.Camera.RotationZ) && !double.IsNaN(m_view.Camera.RotationY) && !double.IsNaN(m_view.Camera.ViewDistance))
            {
                DisableChangedEvents();

                // Check and correct selected value index based on camera.
                if (m_view.Camera.Projection == ProjectionType.Orthographic)
                    comboBoxProjection.SelectedIndex = 0;
                else if (m_view.Camera.Projection == ProjectionType.OrthographicLegacy)
                    comboBoxProjection.SelectedIndex = 1;
                else comboBoxProjection.SelectedIndex = 2; //Perspective. 
                
                if(m_view.Camera.OrientationMode == OrientationModes.XYZ_Mixed)
                    comboBoxCamera.SelectedIndex = 0;
                else comboBoxCamera.SelectedIndex = 1;

                double dRotation = m_view.Camera.RotationX;
                double dChange = 89 * (dRotation < 0 ? -1 : 1);
                dRotation = (Int32)((dRotation + dChange) % 360D - dChange);
                if (dRotation > sliderVerticalRotation.Maximum)
                {
                    dRotation = sliderVerticalRotation.Maximum;
                }
                if (dRotation < sliderVerticalRotation.Minimum)
                {
                    dRotation = sliderVerticalRotation.Minimum;
                }
                sliderVerticalRotation.Value = (Int32)Math.Round(dRotation);
                tbVerticalRotation.Text = sliderVerticalRotation.Value.ToString();


                dRotation = m_view.Camera.RotationY;
                dChange = 179 * (dRotation < 0 ? -1 : 1);
                sliderHorizontalRotation.Value = (Int32)((dRotation + dChange) % 360D - dChange);
                tbHorizontalRotation.Text = sliderHorizontalRotation.Value.ToString();

                dRotation = m_view.Camera.RotationZ;
                dChange = 179 * (dRotation < 0 ? -1 : 1);
                sliderSideRotation.Value = (Int32)((dRotation + dChange) % 360D - dChange);
                tbSideRotation.Text = sliderSideRotation.Value.ToString();
                sliderDistance.Value = (Int32)m_view.Camera.ViewDistance;
                tbDistance.Text = sliderDistance.Value.ToString();
                // set between some min/max values
                int iBoxMin = 10, iBoxMax = 500;
                if ((Int32)m_view.Dimensions.X < iBoxMin)
                {
                    textBoxExWidth.Text = iBoxMin.ToString();
                }
                else if ((Int32)m_view.Dimensions.X > iBoxMax)
                {
                    textBoxExWidth.Text = iBoxMax.ToString();
                }
                else
                {
                    textBoxExWidth.Text = ((int)m_view.Dimensions.X).ToString();
                }

                if ((Int32)m_view.Dimensions.Y < iBoxMin)
                {
                    textBoxExHeight.Text = iBoxMin.ToString();
                }
                else if ((Int32)m_view.Dimensions.Y > iBoxMax)
                {
                    textBoxExHeight.Text = iBoxMax.ToString();
                }
                else
                {
                    textBoxExHeight.Text = ((int)m_view.Dimensions.Y).ToString();
                }

                if ((Int32)m_view.Dimensions.Z < iBoxMin)
                {
                    textBoxExDepth.Text = iBoxMin.ToString();
                }
                else if ((Int32)m_view.Dimensions.Z > iBoxMax)
                {
                    textBoxExDepth.Text = iBoxMax.ToString();
                }
                else
                {
                    textBoxExDepth.Text = ((int)m_view.Dimensions.Z).ToString();
                }

                EnabledChangedEvents();
            }
        }

        /// <summary>
        /// Set all the view point parameters
        /// </summary>
        private void SetViewPoint()
        {
            if (_chart == null || m_view == null)
            {
                return;
            }

            m_bOwnChange = true;

            // Disable rendering, strongly recommended before updating chart properties.
            _chart.BeginUpdate();

            m_view.Camera.SetEulerAngles(sliderVerticalRotation.Value,
                                         sliderHorizontalRotation.Value,
                                         sliderSideRotation.Value);

            m_view.Camera.ViewDistance = sliderDistance.Value;

            //Allow chart rendering
            _chart.EndUpdate();

            m_bOwnChange = false;
            SetControlValuesFromChart();
        }

        /// <summary>
        /// Set view distance.
        /// </summary>
        /// <param name="distance">distance</param>
        public void SetViewDistance(int distance)
        {
            sliderDistance.Value = distance;

            SetViewPoint();
        }

        /// <summary>
        /// Camera view changed, update control values.
        /// </summary>
        /// <param name="newCameraViewPoint">new camera</param>
        /// <param name="view">related 3D view</param>
        /// <param name="chart">related chart</param>
        private void ViewPointEditor_CameraViewChanged(Camera3D newCameraViewPoint, View3D view, LightningChartUltimate chart)
        {
            SetControlValuesFromChart();
        }

        /// <summary>
        /// Camera view changed, update control values.
        /// </summary>
        /// <param name="newCameraViewPoint">new camera</param>
        /// <param name="view">related 3D pie view</param>
        /// <param name="chart">related chart</param>
        private void ViewPointEditor_CameraViewChanged(Camera3D newCameraViewPoint, ViewPie3D view, LightningChartUltimate chart)
        {
            SetControlValuesFromChart();
        }

        /// <summary>
        /// Show view from top.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">arguments</param>
        private void buttonTopView2D_Click(object sender, RoutedEventArgs e)
        {
            DisableChangedEvents();

            // Save old dimensions.
            if (m_view.Camera.Projection != ProjectionType.OrthographicLegacy)
                old_dimensions = saveDimensions();
            else if (m_view.Camera.Projection == ProjectionType.OrthographicLegacy)
                old_dimensions_legacy = saveDimensions();

            // Reset dimensions.
            resetDimensions(old_dimensions);

            // Use orthographic projection for top view.
            m_view.Camera.Projection = ProjectionType.Orthographic;
            comboBoxProjection.SelectedIndex = 0;

            // Set camera orientation mode to mixed.
            m_view.Camera.OrientationMode = OrientationModes.XYZ_Mixed;
            comboBoxCamera.SelectedIndex = 0;

            sliderVerticalRotation.Value = 90;
            sliderHorizontalRotation.Value = 0;
            sliderSideRotation.Value = 90;

            SetViewPoint();

            EnabledChangedEvents();
        }

        /// <summary>
        /// Original view.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">arguments</param>
        private void buttonOriginalView_Click(object sender, RoutedEventArgs e)
        {
            DisableChangedEvents();

            var m_view = _chart.View3D;
            m_view.Camera.RotationX = 20;
            m_view.Camera.RotationY = -25;
            m_view.Camera.RotationZ = 0;
            m_view.Camera.ViewDistance =180;

            m_view.Camera.OrientationMode = OrientationModes.XYZ_Mixed;
            m_view.Camera.Projection = ProjectionType.Perspective;

            EnabledChangedEvents();
        }

        /// <summary>
        /// Rotate camera on x-axis.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">arguments</param>
        private void sliderVerticalRotation_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            m_view.Camera.RotationX = sliderVerticalRotation.Value;
        }

        /// <summary>
        /// Rotate camera on y-axis.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">arguments</param>
        private void sliderHorizontalRotation_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            m_view.Camera.RotationY = sliderHorizontalRotation.Value;
        }

        /// <summary>
        /// Rotate camera on z-axis.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">arguments</param>
        private void sliderSideRotation_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            m_view.Camera.RotationZ = sliderSideRotation.Value;
        }

        /// <summary>
        /// Update camera distance.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">arguments</param>
        private void sliderDistance_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (m_view != null)
            {
                m_view.Camera.ViewDistance = sliderDistance.Value;
            }
        }

        // Variables for saving and resetting old dimension values on projection changes.
        private PointFloatXYZ old_dimensions;
        private PointFloatXYZ old_dimensions_legacy;

        /// <summary>
        /// Select camera projection.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">arguments</param>
        private void comboBoxProjection_SelectionChanged(object sender, EventArgs e)
        {
            if (m_view != null)
            {
                if (m_view.Camera.Projection != ProjectionType.OrthographicLegacy)
                    old_dimensions = saveDimensions();
                else if (m_view.Camera.Projection == ProjectionType.OrthographicLegacy)
                    old_dimensions_legacy = saveDimensions();

                if (comboBoxProjection.SelectedIndex >= 0)
                {
                    m_view.Camera.Projection = (ProjectionType)comboBoxProjection.SelectedIndex;
                    if (m_view.Camera.Projection != ProjectionType.OrthographicLegacy)
                    {
                        sliderDistance.IsEnabled = true;
                        resetDimensions(old_dimensions);
                    }
                    else if (m_view.Camera.Projection == ProjectionType.OrthographicLegacy)
                    {
                        sliderDistance.IsEnabled = false; // Disable distance slider on old orthographic projection.
                        if(old_dimensions_legacy != null)
                            resetDimensions(old_dimensions_legacy);
                    }
                }
            }
        }

        /// <summary>
        /// Used for saving and returning old dimension values.
        /// </summary>
        private PointFloatXYZ saveDimensions()
        {
            if (m_view != null)
            {
                PointFloatXYZ points = new PointFloatXYZ();
                points.X = m_view.Dimensions.X;
                points.Y = m_view.Dimensions.Y;
                points.Z = m_view.Dimensions.Z;

                return points;
            }
            else
                return null;

        }

        /// <summary>
        /// Resets the dimension scaling caused by using old orthographic projection when using non-dimension scaling projections.
        /// </summary>
        /// <param name="dimensions"></param>
        private void resetDimensions(PointFloatXYZ dimensions)
        {
            if (m_view != null)
            {
                _chart.BeginUpdate();
                m_view.Dimensions.X = dimensions.X;
                m_view.Dimensions.Y = dimensions.Y;
                m_view.Dimensions.Z = dimensions.Z;
                _chart.EndUpdate();
            }

            SetControlValuesFromChart();
        }

        private void textBoxExWidth_TextChanged(Object sender, String oldValue, String newValue)
        {
            if (m_view != null)
            {
                try
                {
                    Single value = Single.Parse(textBoxExWidth.Text);
                    m_view.Dimensions.X = value;
                }
                catch
                {
                    return;
                }
            }
        }

        private void textBoxExHeight_TextChanged(Object sender, String oldValue, String newValue)
        {
            if (m_view != null)
            {
                try
                {
                    Single value = Single.Parse(textBoxExHeight.Text);
                    m_view.Dimensions.Y = value;
                }
                catch
                {
                    return;
                }
            }
        }

        /// <summary>
        /// Update z-dimension.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">arguments</param>
        private void textBoxExDepth_TextChanged(Object sender, String oldValue, String newValue)
        {
            if (m_view != null)
            {
                try
                {
                    Single value = Single.Parse(textBoxExDepth.Text);
                    m_view.Dimensions.Z = value;
                }
                catch
                {
                    return;
                }
            }
        }

        private void ComboBoxCamera_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (m_view != null)
            {
                if (comboBoxCamera.SelectedIndex == 0 && m_view.Camera.OrientationMode != OrientationModes.XYZ_Mixed)
                {
                    m_view.Camera.OrientationMode = OrientationModes.XYZ_Mixed;
                }
                else if (comboBoxCamera.SelectedIndex == 1 && m_view.Camera.OrientationMode != OrientationModes.ZXY_Extrinsic)
                {
                    m_view.Camera.OrientationMode = OrientationModes.ZXY_Extrinsic;
                }
            }
        }

        private void TbVerticalRotation_TextChanged(Object sender, String oldValue, String newValue)
        {
            if (m_view != null && oldValue != newValue)
            {
                try
                {
                    Single value = Single.Parse(tbVerticalRotation.Text);
                    m_view.Camera.RotationX = value;
                }
                catch
                {
                    return;
                }
            }
        }

        private void TbHorizontalRotation_TextChanged(Object sender, String oldValue, String newValue)
        {
            if (m_view != null && oldValue != newValue)
            {
                try
                {
                    Single value = Single.Parse(tbHorizontalRotation.Text);
                    m_view.Camera.RotationY = value;
                }
                catch
                {
                    return;
                }
            }
        }

        private void TbSideRotation_TextChanged(Object sender, String oldValue, String newValue)
        {
            if (m_view != null && oldValue != newValue)
            {
                try
                {
                    Single value = Single.Parse(tbSideRotation.Text);
                    m_view.Camera.RotationZ = value;
                }
                catch
                {
                    return;
                }
            }
        }

        private void TbDistance_TextChanged(Object sender, String oldValue, String newValue)
        {
            if (m_view != null && oldValue != newValue)
            {
                try
                {
                    Single value = Single.Parse(tbDistance.Text);
                    m_view.Camera.ViewDistance = value;
                }
                catch
                {
                    return;
                }
            }
        }

       
    }
}
