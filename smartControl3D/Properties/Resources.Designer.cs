﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace smartControl3D.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("smartControl3D.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot; ?&gt;
        ///
        ///&lt;Translation&gt;
        ///  
        ///  &lt;Translate ID=&quot;buttonOriginalView&quot;&gt;
        ///    &lt;ru&gt;Исходный вид&lt;/ru&gt;
        ///    &lt;en&gt;Original View&lt;/en&gt;
        ///  &lt;/Translate&gt;
        ///
        ///  &lt;Translate ID=&quot;buttonTopView2D&quot;&gt;
        ///    &lt;ru&gt;Вид сверху&lt;/ru&gt;
        ///    &lt;en&gt;Top view 2D&lt;/en&gt;
        ///  &lt;/Translate&gt;
        ///
        ///  &lt;Translate ID=&quot;label2&quot;&gt;
        ///    &lt;ru&gt;Ширина&lt;/ru&gt;
        ///    &lt;en&gt;Width&lt;/en&gt;
        ///  &lt;/Translate&gt;
        ///
        ///  &lt;Translate ID=&quot;label3&quot;&gt;
        ///    &lt;ru&gt;Высота&lt;/ru&gt;
        ///    &lt;en&gt;Height&lt;/en&gt;
        ///  &lt;/Translate&gt;
        ///
        ///  &lt;Translate ID=&quot;label4&quot;&gt;
        ///    &lt;ru&gt;Глубина&lt;/ru&gt;
        ///    &lt;en&gt;Dep [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string Translation {
            get {
                return ResourceManager.GetString("Translation", resourceCulture);
            }
        }
    }
}
