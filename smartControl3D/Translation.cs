﻿using Arction.Wpf.SemibindableCharting.Series3D;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Xml;

namespace smartControl3D
{
    public partial class Control3D : UserControl
    {
        private Languages _CurrentLanguage = Languages.EN;
        public Languages CurrentLanguage
        {
            get
            {
                return _CurrentLanguage;
            }
            set
            {
                if (_CurrentLanguage != value)
                {
                    _CurrentLanguage = value;
                    ReTranslateAllControls();
                }
            }
        }

        public string Position { get; private set; } = "Position";
        public string Trajectory { get; private set; } = "Trajectory";


        public enum Languages : byte
        {
            [Description("Русский")]
            RU,
            [Description("English")]
            EN
        }

        private Dictionary<string, string> LoadDictionary(Languages languages)
        {
            var translation = Properties.Resources.Translation;
            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(translation);

            Dictionary<string, string> TranslateDictionary = new Dictionary<string, string>();
            // получим корневой элемент
            XmlElement xRoot = xDoc.DocumentElement;
            foreach (XmlNode x2Node in xRoot.ChildNodes)
            {
                if (x2Node.NodeType == XmlNodeType.Comment)
                    continue;

                // получаем атрибут ID
                if (x2Node.Attributes.Count > 0)
                {
                    XmlNode attr = x2Node.Attributes.GetNamedItem("ID");
                    if (attr != null)
                    {
                        foreach (XmlNode childnode in x2Node.ChildNodes)
                        {
                            // если узел - language
                            if (childnode.Name == languages.ToString().ToLower())
                            {
                                if (!TranslateDictionary.ContainsKey(attr.Value))
                                    TranslateDictionary.Add(attr.Value, childnode.InnerText);
                            }
                        }
                    }
                }
            }

            return TranslateDictionary;
        }

        private void ReTranslateAllControls()
        {
            Dictionary<string, string> TranslateDictionary = LoadDictionary(CurrentLanguage);

            {
                //foreach (var children in myGrid.Children)
                //{
                //    if (children is StackPanel)
                //    {
                //        StackPanel temp = (StackPanel)children;
                //        RenameControls(temp.Children, TranslateDictionary);
                //    }
                //}
            }

            RenameControls(myGrid.Children, TranslateDictionary);

            RenameControls(viewPointEditor1.VPEGrid.Children, TranslateDictionary);

            TranslateProperties(TranslateDictionary);

            ReTranslateLegend();
        }

        private void TranslateProperties(Dictionary<string, string> TranslateDictionary)
        {
            {/*
            if (TranslateDictionary.ContainsKey(nameof(Position)))
                Position = TranslateDictionary[nameof(Position)];

            if (TranslateDictionary.ContainsKey(nameof(Trajectory)))
                Trajectory = TranslateDictionary[nameof(Trajectory)];
            */
            }

            var properties = typeof(Control3D).GetProperties();
            foreach (var prop in properties)
            {
                if (prop.PropertyType == typeof(string))
                {
                    if (TranslateDictionary.ContainsKey(prop.Name))
                    {
                        prop.SetValue(this, TranslateDictionary[prop.Name]);
                    }
                }
            }
        }

        private void ReTranslateLegend()
        {
            for (int i = 0; i < IDcountTrajOld.Count; i++)
            {
                int ID = IDcountTrajOld.ElementAt(i).Key;
                _chart.View3D.PointLineSeries3D[2 * i].Title.Text = $"ID: {ID} {Position}";
                _chart.View3D.PointLineSeries3D[2 * i + 1].Title.Text = $"ID: {ID} {Trajectory}";
            }

            for (int i = 0; i < IDcountTrajOldAero.Count; i++)
            {
                string SN = IDcountTrajOldAero.ElementAt(i).Key;
                _chart.View3D.PointLineSeries3D[2 * IDcountTrajOld.Count + 2 * i].Title.Text = $"SN: {SN} {Position}";
                _chart.View3D.PointLineSeries3D[2 * IDcountTrajOld.Count + 2 * i + 1].Title.Text = $"SN: {SN} {Trajectory}";
            }
        }

        private void RenameControls(UIElementCollection Childrens, Dictionary<string, string> TranslateDictionary)
        {
            foreach (var control in Childrens)
            {
                if (control is Button)
                {
                    var b = (Button)control;

                    if (TranslateDictionary.ContainsKey(b.Name))
                        b.Content = TranslateDictionary[b.Name];

                    if (TranslateDictionary.ContainsKey(b.Name + ".ToolTip"))
                        b.ToolTip = TranslateDictionary[b.Name + ".ToolTip"];
                }
                if (control is ToggleButton)
                {
                    var tb = (ToggleButton)control;

                    if (TranslateDictionary.ContainsKey(tb.Name))
                        tb.Content = TranslateDictionary[tb.Name];

                    if (TranslateDictionary.ContainsKey(tb.Name + ".ToolTip"))
                        tb.ToolTip = TranslateDictionary[tb.Name + ".ToolTip"];
                }
                if (control is Label)
                {
                    var l = (Label)control;

                    if (TranslateDictionary.ContainsKey(l.Name))
                        l.Content = TranslateDictionary[l.Name];

                    if (TranslateDictionary.ContainsKey(l.Name + ".ToolTip"))
                        l.ToolTip = TranslateDictionary[l.Name + ".ToolTip"];
                }
                if (control is ComboBox)
                {
                    var cb = (ComboBox)control;
                    foreach (var item in cb.Items)
                    {
                        if (item is ComboBoxItem)
                        {
                            ComboBoxItem cbi = (ComboBoxItem)item;

                            if (TranslateDictionary.ContainsKey(cbi.Name))
                                cbi.Content = TranslateDictionary[cbi.Name];

                            if (TranslateDictionary.ContainsKey(cbi.Name + ".ToolTip"))
                                cbi.ToolTip = TranslateDictionary[cbi.Name + ".ToolTip"];
                        }
                    }
                }
                if (control is CheckBox)
                {
                    var cb = (CheckBox)control;

                    if (TranslateDictionary.ContainsKey(cb.Name))
                        cb.Content = TranslateDictionary[cb.Name];

                    if (TranslateDictionary.ContainsKey(cb.Name + ".ToolTip"))
                        cb.ToolTip = TranslateDictionary[cb.Name + ".ToolTip"];
                }
                if (control is GroupBox)
                {
                    var gb = (GroupBox)control;

                    if (TranslateDictionary.ContainsKey(gb.Name))
                        gb.Header = TranslateDictionary[gb.Name];

                    if (TranslateDictionary.ContainsKey(gb.Name + ".ToolTip"))
                        gb.ToolTip = TranslateDictionary[gb.Name + ".ToolTip"];

                    if (gb.Content is Grid)
                    {
                        var g = (Grid)gb.Content;

                        RenameControls(g.Children, TranslateDictionary);
                    }

                    if (gb.Content is StackPanel)
                    {
                        var sp = (StackPanel)control;

                        RenameControls(sp.Children, TranslateDictionary);
                    }
                }

                if (control is Grid)
                {
                    var g = (Grid)control;

                    RenameControls(g.Children, TranslateDictionary);
                }
                if (control is StackPanel)
                {
                    var sp = (StackPanel)control;

                    RenameControls(sp.Children, TranslateDictionary);
                }
            }

        }
    }
}
