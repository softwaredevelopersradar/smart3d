﻿using Arction.Wpf.SemibindableCharting;
using Arction.Wpf.SemibindableCharting.Annotations;
using Arction.Wpf.SemibindableCharting.Series3D;
using Arction.Wpf.SemibindableCharting.Views.View3D;
using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Color = System.Windows.Media.Color;

namespace smartControl3D
{
    public partial class Control3D
    {
        //Класс Расчета координат
        fDRM.ClassDRM_dll classDRM_Dll = new fDRM.ClassDRM_dll();

        float _LengthTrack = 1500;
        public float LengthTrack
        {
            get { return _LengthTrack; }
            set
            {
                if (_LengthTrack != value)
                {
                    _LengthTrack = value;
                }
            }
        }

        List<Coord> Track(List<Coord> Coords)
        {
            List<Coord> track = new List<Coord>();

            float accLengthTrack = 0;

            for (int i = Coords.Count() - 1; i >= 0; i--)
            {
                if (i == Coords.Count() - 1)
                {
                    track.Insert(0, Coords[i]);
                }
                else
                {
                    var d = classDRM_Dll.f_D_2Points(
                                         Coords[i+1].Latitude,
                                         Coords[i+1].Longitude,
                                         Coords[i].Latitude,
                                        Coords[i].Longitude,
                                          1);
                    if (accLengthTrack + d <= LengthTrack)
                    {
                        accLengthTrack = accLengthTrack + (float)d;
                        track.Insert(0, Coords[i]);
                    }
                    else
                    {
                        return track;
                    }

                }
            }

            return track;
        }

        private int ActiveUAVcount = 0;
        private int ActiveAeroCount = 0;

        private List<PointLineSeries3D> lUAVPLS = new List<PointLineSeries3D>();
        private List<PointLineSeries3D> lAeroPLS = new List<PointLineSeries3D>();

        public void Plot2(List<TableUAVRes> TablesUAVResList, List<TableUAVTrajectory> TableUAVTrajectoryList)
        {
            ////Очистить траектории
            //_chart.View3D.PointLineSeries3D.Clear();

            //Получить ListId
            List<int> ListID = TablesUAVResList.Select(x => x.Id).ToList();

            ColorWork(ListID);

            //Вернуть почти всё как было
            Default(ListID.Count());

            ActiveUAVcount = ListID.Count();

            lUAVPLS.Clear();

            for (int i = 0; i < ListID.Count(); i++)
            {
                //Получить координаты дрона из траекторий

                //Получить ID для работы
                int currentID = ListID[i];

                //Получаем запись по дрону для работы
                TableUAVRes currentDrone = TablesUAVResList.Where(x => x.Id == currentID).ToList().FirstOrDefault();

                //Получаем траекторию по ID
                List<TableUAVTrajectory> selectCurrentTraj = TableUAVTrajectoryList.Where(x => x.TableUAVResId == currentID).ToList();

                if (selectCurrentTraj.Count > 0)
                {
                    int MaxNum = selectCurrentTraj.Max(x => x.Num);

                    //Получаем крайние координаты
                    Coord lastCoords = selectCurrentTraj.Where(x => x.Num == MaxNum).Select(x => x.Coordinates).FirstOrDefault();

                    //Получаем крайнюю частоту
                    double lastFreq = selectCurrentTraj.Where(x => x.Num == MaxNum).Select(x => x.FrequencyKHz).FirstOrDefault();

                    //Разместить дрон по координатам
                    SetDrone(i + 1, lastCoords);

                    //Разместить туда же аннотацию с хитрым текстом
                    SetСraftyAnnotation(i, lastCoords, lastFreq, currentDrone);

                    //Сделать траектории
                    //PointLineSeries3D newDataSeries = MakeTraj();
                    //PointLineSeries3D newDataSeries = MakeTrajCustom(currentID, ColorPallete(i));
                    PointLineSeries3D newDataSeries = MakeTrajCustom(currentID, ColorPallete(ColorIDPicker[currentID]));

                    PointLineSeries3D newDataSeries2 = MakeTraj2Custom(currentID);

                    //Получить все координаты
                    List<Coord> Coords = selectCurrentTraj.Select(x => x.Coordinates).ToList();

                    //Заполнить траектории
                    newDataSeries.Points = Points3DFromCoords(Coords);
                    newDataSeries2.Points = Points3DFromCoords2(Coords);

                    //Добавить траеториии на 3D
                    //_chart.View3D.PointLineSeries3D.Add(newDataSeries);
                    //_chart.View3D.PointLineSeries3D.Add(newDataSeries2);

                    ////Вставить траектории в нужное место
                    //_chart.View3D.PointLineSeries3D.Insert(i, newDataSeries);
                    //_chart.View3D.PointLineSeries3D.Insert(i + 1, newDataSeries2);

                    //А если попробовать через лист
                    lUAVPLS.Add(newDataSeries);
                    lUAVPLS.Add(newDataSeries2);
                }
            }
            _chart.View3D.PointLineSeries3D.AddRange(lUAVPLS);
            _chart.View3D.PointLineSeries3D.AddRange(lAeroPLS);

            SeriesPoint3D[] Points3DFromCoords(List<Coord> coords)
            {
                List<SeriesPoint3D> lseriesPoint3Ds = new List<SeriesPoint3D>();

                for (int i = 0; i < coords.Count; i++)
                {
                    lseriesPoint3Ds.Add(new SeriesPoint3D(coords[i].Latitude, coords[i].Altitude, coords[i].Longitude));
                }
                return lseriesPoint3Ds.ToArray();
            }
            SeriesPoint3D[] Points3DFromCoords2(List<Coord> coords)
            {
                List<SeriesPoint3D> lseries2Point3Ds = new List<SeriesPoint3D>();

                for (int i = 0; i < coords.Count; i++)
                {
                    lseries2Point3Ds.Add(new SeriesPoint3D(coords[i].Latitude, coords[i].Altitude, coords[i].Longitude));
                    lseries2Point3Ds.Add(new SeriesPoint3D(coords[i].Latitude, 0, coords[i].Longitude));
                    lseries2Point3Ds.Add(new SeriesPoint3D(coords[i].Latitude, coords[i].Altitude, coords[i].Longitude));
                }

                return lseries2Point3Ds.ToArray();
            }

            PointLineSeries3D MakeTraj()
            {
                PointLineSeries3D newDataSeries = new PointLineSeries3D()
                {
                    XAxisBinding = Axis3DBinding.Primary,
                    YAxisBinding = Axis3DBinding.Primary,
                    ZAxisBinding = Axis3DBinding.Primary,
                    MouseInteraction = false,
                    MouseHighlight = MouseOverHighlight.None,
                };

                newDataSeries.PointsVisible = false;
                newDataSeries.LineVisible = true;
                newDataSeries.LineStyle.Width = 0.3f;
                newDataSeries.Title.Text = "Position";
                newDataSeries.MouseInteraction = false;
                newDataSeries.LineStyle.Color = System.Windows.Media.Color.FromRgb(255, 0, 0);

                return newDataSeries;
            }
            PointLineSeries3D MakeTrajCustom(int ID, Color color)
            {
                PointLineSeries3D newDataSeries = new PointLineSeries3D()
                {
                    XAxisBinding = Axis3DBinding.Primary,
                    YAxisBinding = Axis3DBinding.Primary,
                    ZAxisBinding = Axis3DBinding.Primary,
                    MouseInteraction = false,
                    MouseHighlight = MouseOverHighlight.None,
                };

                newDataSeries.PointsVisible = false;
                newDataSeries.LineVisible = true;
                newDataSeries.LineStyle.Width = 0.3f;
                newDataSeries.Title.Text = $"ID: {ID} Position";
                newDataSeries.MouseInteraction = false;
                newDataSeries.LineStyle.Color = color;

                return newDataSeries;
            }
            PointLineSeries3D MakeTraj2()
            {
                PointLineSeries3D newDataSeries2 = new PointLineSeries3D()
                {
                    XAxisBinding = Axis3DBinding.Primary,
                    YAxisBinding = Axis3DBinding.Primary,
                    ZAxisBinding = Axis3DBinding.Primary,
                    MouseInteraction = false,
                    MouseHighlight = MouseOverHighlight.None,
                };

                newDataSeries2.PointsVisible = false;
                newDataSeries2.LineVisible = true;
                newDataSeries2.LineStyle.Width = 0.1f;
                newDataSeries2.Title.Text = "Trajectory";
                newDataSeries2.MouseInteraction = false;
                newDataSeries2.LineStyle.Color = System.Windows.Media.Color.FromRgb(255, 255, 0);

                return newDataSeries2;
            }
            PointLineSeries3D MakeTraj2Custom(int ID)
            {
                PointLineSeries3D newDataSeries2 = new PointLineSeries3D()
                {
                    XAxisBinding = Axis3DBinding.Primary,
                    YAxisBinding = Axis3DBinding.Primary,
                    ZAxisBinding = Axis3DBinding.Primary,
                    MouseInteraction = false,
                    MouseHighlight = MouseOverHighlight.None,
                };

                newDataSeries2.PointsVisible = false;
                newDataSeries2.LineVisible = true;
                newDataSeries2.LineStyle.Width = 0.1f;
                newDataSeries2.Title.Text = $"ID: {ID} Trajectory";
                newDataSeries2.MouseInteraction = false;
                newDataSeries2.LineStyle.Color = System.Windows.Media.Color.FromRgb(255, 255, 0);

                return newDataSeries2;
            }
            PointLineSeries3D MakeTraj2Color(Color color)
            {
                PointLineSeries3D newDataSeries2 = new PointLineSeries3D()
                {
                    XAxisBinding = Axis3DBinding.Primary,
                    YAxisBinding = Axis3DBinding.Primary,
                    ZAxisBinding = Axis3DBinding.Primary,
                    MouseInteraction = false,
                    MouseHighlight = MouseOverHighlight.None,
                };

                newDataSeries2.PointsVisible = false;
                newDataSeries2.LineVisible = true;
                newDataSeries2.LineStyle.Width = 0.1f;
                newDataSeries2.Title.Text = "Trajectory";
                newDataSeries2.MouseInteraction = false;
                newDataSeries2.LineStyle.Color = color;

                return newDataSeries2;
            }

            void SetDrone(int index, Coord coord)
            {
                _chart.View3D.MeshModels[index].Position.X = (float)coord.Latitude;
                _chart.View3D.MeshModels[index].Position.Y = (float)coord.Altitude;
                _chart.View3D.MeshModels[index].Position.Z = (float)coord.Longitude;
            }

        }

        Dictionary<int, int> IDcountTrajOld = new Dictionary<int, int>();

        Dictionary<int, int> IDcountTrajNew = new Dictionary<int, int>();

        private bool AllisnotOK(Dictionary<int, int> checkDicOld, Dictionary<int, int> checkDicNew)
        {
            if (checkDicOld.Count != checkDicNew.Count) return true;

            for (int i = 0; i < checkDicOld.Count; i++)
            {
                if (checkDicOld.ElementAt(i).Key != checkDicNew.ElementAt(i).Key) return true;
                if (checkDicOld.ElementAt(i).Value != checkDicNew.ElementAt(i).Value) return true;
            }

            return false;
        }
        private int Check(Dictionary<int, int> checkDicOld, Dictionary<int, int> checkDicNew, int index)
        {
            if (checkDicOld.Count() - 1 < index && index <= checkDicNew.Count() - 1) return 1;

            if (checkDicNew.Count() - 1 < index && index <= checkDicOld.Count() - 1) return 2;

            if (checkDicOld.ElementAt(index).Key != checkDicNew.ElementAt(index).Key) return 3;
            if (checkDicOld.ElementAt(index).Value != checkDicNew.ElementAt(index).Value) return 4;

            return 0;
        }

        private static SemaphoreSlim semaphoreSlim = new SemaphoreSlim(1, 1);

        public void Plot3(List<TableUAVRes> TablesUAVResList, List<TableUAVTrajectory> TableUAVTrajectoryList)
        {
            semaphoreSlim.Wait();

            //Получить ListId
            List<int> ListID = TablesUAVResList.Select(x => x.Id).ToList();

            //В любом случае заполняем что пришло
            for (int i = 0; i < ListID.Count(); i++)
            {
                //Получить ID для работы
                int currentID = ListID[i];

                //Получаем кол-во точек траектории по ID
                int countTraj = TableUAVTrajectoryList.Where(x => x.TableUAVResId == currentID).ToList().Count();

                if (countTraj > 0)
                {
                    //Добавляем в Словарь ново-пришедших
                    IDcountTrajNew.Add(currentID, countTraj);
                }
            }

            //Теперь будет лист уже упорядоченный после словаря
            ListID = IDcountTrajNew.Keys.ToList();

            ColorWork(ListID);

            while (AllisnotOK(IDcountTrajOld, IDcountTrajNew))
            {
                if (IDcountTrajOld.Count == 0)
                {
                    //Добавляем всё что есть
                    for (int i = 0; i < IDcountTrajNew.Count(); i++)
                    {
                        InsertRecord(IDcountTrajNew.ElementAt(i).Key, i);
                        IDcountTrajOld.Add(IDcountTrajNew.ElementAt(i).Key, IDcountTrajNew.ElementAt(i).Value);
                    }
                }
                else
                {
                    //суперски сравниваем чё кого пришло и решаем что по жизни

                    //Для начала сравниваем по кол-ву
                    if (IDcountTrajOld.Count == IDcountTrajNew.Count)
                    {
                        //если кол-во одинаково, может там где добавились точки траектории
                        for (int i = 0; i < IDcountTrajNew.Count(); i++)
                        {
                            switch (Check(IDcountTrajOld, IDcountTrajNew, i))
                            {
                                case 0:
                                    break;
                                case 1:
                                    break;
                                case 2:
                                    break;
                                case 3:
                                    break;
                                case 4:
                                    //AddPointsTraj(IDcountTrajNew.ElementAt(i).Key, IDcountTrajNew.ElementAt(i).Value - IDcountTrajOld.ElementAt(i).Value, i);
                                    AddPointsTraj2(IDcountTrajNew.ElementAt(i).Key, IDcountTrajNew.ElementAt(i).Value - IDcountTrajOld.ElementAt(i).Value, i);

                                    IDcountTrajOld.Remove(IDcountTrajNew.ElementAt(i).Key);
                                    if (IDcountTrajOld.Count == 0) IDcountTrajOld.Clear();
                                    IDcountTrajOld.Add(IDcountTrajNew.ElementAt(i).Key, IDcountTrajNew.ElementAt(i).Value);
                                    break;
                            }
                        }
                    }
                    else
                    {
                        //если кол-во неравно, то может быть больше, а может и меньше
                        if (IDcountTrajOld.Count < IDcountTrajNew.Count)
                        {
                            for (int i = 0; i < IDcountTrajNew.Count(); i++)
                            {
                                switch (Check(IDcountTrajOld, IDcountTrajNew, i))
                                {
                                    case 0:
                                        break;
                                    case 1:
                                        InsertRecord(IDcountTrajNew.ElementAt(i).Key, i);
                                        IDcountTrajOld.Add(IDcountTrajNew.ElementAt(i).Key, IDcountTrajNew.ElementAt(i).Value);
                                        break;
                                    case 2:
                                        break;
                                    case 3:
                                        InsertRecord(IDcountTrajNew.ElementAt(i).Key, i);
                                        IDcountTrajOld.Add(IDcountTrajNew.ElementAt(i).Key, IDcountTrajNew.ElementAt(i).Value);
                                        break;
                                    case 4:
                                        break;
                                }
                            }
                        }
                        if (IDcountTrajOld.Count > IDcountTrajNew.Count)
                        {
                            int localCountTrajOld = IDcountTrajOld.Count();
                            for (int i = 0, iterations = 0; i < IDcountTrajOld.Count(); i++, iterations++)
                                {
                                switch (Check(IDcountTrajOld, IDcountTrajNew, i))
                                {
                                    case 0:
                                        break;
                                    case 1:
                                        break;
                                    case 2:
                                        //RemoveRecord(i);
                                        RemoveRecord2(iterations,i);
                                        IDcountTrajOld.Remove(IDcountTrajOld.ElementAt(i).Key);
                                        if (iterations < localCountTrajOld) i--;
                                        if (IDcountTrajOld.Count == 0) IDcountTrajOld.Clear();
                                        break;
                                    case 3:
                                        RemoveRecord(i);
                                        IDcountTrajOld.Remove(IDcountTrajOld.ElementAt(i).Key);
                                        if (IDcountTrajOld.Count == 0) IDcountTrajOld.Clear();
                                        break;
                                    case 4:
                                        break;
                                }
                            }
                        }
                    }
                }
            }

            IDcountTrajNew.Clear();

            semaphoreSlim.Release();


            void InsertRecord(int currentID, int index)
            {
                //Получаем запись по дрону для работы
                TableUAVRes currentDrone = TablesUAVResList.Where(x => x.Id == currentID).ToList().FirstOrDefault();

                //Получаем траекторию по ID
                List<TableUAVTrajectory> selectCurrentTraj = TableUAVTrajectoryList.Where(x => x.TableUAVResId == currentID).OrderBy(x=>x.Num).ToList();

                if (selectCurrentTraj.Count > 0)
                {
                    int MaxNum = selectCurrentTraj.Max(x => x.Num);

                    //Получаем крайние координаты
                    Coord lastCoords = selectCurrentTraj.Where(x => x.Num == MaxNum).Select(x => x.Coordinates).FirstOrDefault();
                    //получаем крайний Elevation
                    lastCoords.Altitude = (lastCoords.Altitude == 0) ? (selectCurrentTraj.Where(x => x.Num == MaxNum).Select(x => x.Elevation).FirstOrDefault() >= 0) ? selectCurrentTraj.Where(x => x.Num == MaxNum).Select(x => x.Elevation).FirstOrDefault() : 0 : lastCoords.Altitude;

                    //Получаем крайнюю частоту
                    double lastFreq = selectCurrentTraj.Where(x => x.Num == MaxNum).Select(x => x.FrequencyKHz).FirstOrDefault();

                    //Разместить дрон по координатам
                    SetDrone(index + 1, lastCoords);

                    //Разместить туда же аннотацию с хитрым текстом
                    SetСraftyAnnotation(index, lastCoords, lastFreq, currentDrone);

                    //Сделать траектории
                    PointLineSeries3D newDataSeries = MakeTrajCustom(currentID, ColorPallete(ColorIDPicker[currentID]));
                    PointLineSeries3D newDataSeries2 = MakeTraj2Custom(currentID, Colors.Yellow);

                    //Получить все координаты
                    List<Coord> Coords = selectCurrentTraj.Select(x => x.Coordinates).ToList();
                    //получить все Elevation
                    List<float> Elevations = selectCurrentTraj.Select(x => x.Elevation).ToList();
                    for (int i = 0; i < Coords.Count(); i++)
                    {
                        Coords[i].Altitude = (Coords[i].Altitude == 0) ? (Elevations[i] >= 0) ? Elevations[i] : 0 : Coords[i].Altitude;
                    }

                    //Получить хвост длинной не более TrackLenght
                    List<Coord> trackCoords = Track(Coords);

                    //Заполнить траектории
                    //newDataSeries.Points = Points3DFromCoords(Coords);
                    //newDataSeries2.Points = Points3DFromCoords2(Coords);

                    //Заполнить только хвост траекторий
                    newDataSeries.Points = Points3DFromCoords(trackCoords);
                    newDataSeries2.Points = Points3DFromCoords2(trackCoords);

                    //Вставить траектории на 3D в нужное место
                    int dIndex = index * 2;
                    _chart.View3D.PointLineSeries3D.Insert(dIndex, newDataSeries);
                    _chart.View3D.PointLineSeries3D.Insert(dIndex + 1, newDataSeries2);
                }

                SeriesPoint3D[] Points3DFromCoords(List<Coord> coords)
                {
                    List<SeriesPoint3D> lseriesPoint3Ds = new List<SeriesPoint3D>();

                    for (int i = 0; i < coords.Count; i++)
                    {
                        lseriesPoint3Ds.Add(new SeriesPoint3D(coords[i].Latitude, coords[i].Altitude, coords[i].Longitude));
                    }
                    return lseriesPoint3Ds.ToArray();
                }
                SeriesPoint3D[] Points3DFromCoords2(List<Coord> coords)
                {
                    List<SeriesPoint3D> lseries2Point3Ds = new List<SeriesPoint3D>();

                    for (int i = 0; i < coords.Count; i++)
                    {
                        lseries2Point3Ds.Add(new SeriesPoint3D(coords[i].Latitude, coords[i].Altitude, coords[i].Longitude));
                        lseries2Point3Ds.Add(new SeriesPoint3D(coords[i].Latitude, 0, coords[i].Longitude));
                        lseries2Point3Ds.Add(new SeriesPoint3D(coords[i].Latitude, coords[i].Altitude, coords[i].Longitude));
                    }

                    return lseries2Point3Ds.ToArray();
                }

               

                void SetDrone(int setDroneIndex, Coord coord)
                {
                    _chart.View3D.MeshModels[setDroneIndex].Position.X = (float)coord.Latitude;
                    _chart.View3D.MeshModels[setDroneIndex].Position.Y = (float)coord.Altitude;
                    _chart.View3D.MeshModels[setDroneIndex].Position.Z = (float)coord.Longitude;
                }
            }

            void RemoveRecord(int index)
            {
                //Вернуть UAVдрон на место
                _chart.View3D.MeshModels[1 + index].Position.X = 0;
                _chart.View3D.MeshModels[1 + index].Position.Y = 0;
                _chart.View3D.MeshModels[1 + index].Position.Z = 0;

                //Вернуть UAVаннотацию на место
                _chart.View3D.Annotations[index].TargetAxisValues.SetValues(0, 0, 0);
                _chart.View3D.Annotations[index].Text = "";

                //Убрать траекторию
                int killIndex = index * 2;
                _chart.View3D.PointLineSeries3D.RemoveAt(killIndex);
                _chart.View3D.PointLineSeries3D.RemoveAt(killIndex);
            }

            void RemoveRecord2(int indexRemoveDrone, int indexRemoveTraj)
            {
                //Вернуть UAVдрон на место
                _chart.View3D.MeshModels[1 + indexRemoveDrone].Position.X = 0;
                _chart.View3D.MeshModels[1 + indexRemoveDrone].Position.Y = 0;
                _chart.View3D.MeshModels[1 + indexRemoveDrone].Position.Z = 0;

                //Вернуть UAVаннотацию на место
                _chart.View3D.Annotations[indexRemoveDrone].TargetAxisValues.SetValues(0, 0, 0);
                _chart.View3D.Annotations[indexRemoveDrone].Text = "";

                //Убрать траекторию
                int killIndex = indexRemoveTraj * 2;
                _chart.View3D.PointLineSeries3D.RemoveAt(killIndex);
                _chart.View3D.PointLineSeries3D.RemoveAt(killIndex);
            }

            void AddPointsTraj(int currentID, int dif, int index)
            {
                //Получаем запись по дрону для работы
                TableUAVRes currentDrone = TablesUAVResList.Where(x => x.Id == currentID).ToList().FirstOrDefault();

                //Получаем траекторию по ID
                List<TableUAVTrajectory> selectCurrentTraj = TableUAVTrajectoryList.Where(x => x.TableUAVResId == currentID).ToList();

                if (selectCurrentTraj.Count > 0)
                {
                    int MaxNum = selectCurrentTraj.Max(x => x.Num);

                    //Получаем крайние координаты
                    Coord lastCoords = selectCurrentTraj.Where(x => x.Num == MaxNum).Select(x => x.Coordinates).FirstOrDefault();

                    //Получаем крайнюю частоту
                    double lastFreq = selectCurrentTraj.Where(x => x.Num == MaxNum).Select(x => x.FrequencyKHz).FirstOrDefault();

                    //Разместить дрон по координатам
                    SetDrone(index + 1, lastCoords);

                    //Разместить туда же аннотацию с хитрым текстом
                    SetСraftyAnnotation(index, lastCoords, lastFreq, currentDrone);

                    //Лист для крайних координат
                    List<Coord> Coords = new List<Coord>();

                    //Заполнить его крайними координатами
                    for (int w = selectCurrentTraj.Count() - dif; w < selectCurrentTraj.Count(); w++)
                    {
                        Coords.Add(selectCurrentTraj[w].Coordinates);
                    }

                    //Заполнить точки в нужном виде
                    var Points = Points3DFromCoords(Coords);
                    var Points2 = Points3DFromCoords2(Coords);

                    //Добавить точки к существующее траектории
                    _chart.BeginUpdate();
                    _chart.View3D.PointLineSeries3D[index].AddPoints(Points, true);
                    _chart.View3D.PointLineSeries3D[index+1].AddPoints(Points2, true);
                    _chart.EndUpdate();
                }

                SeriesPoint3D[] Points3DFromCoords(List<Coord> coords)
                {
                    List<SeriesPoint3D> lseriesPoint3Ds = new List<SeriesPoint3D>();

                    for (int i = 0; i < coords.Count; i++)
                    {
                        lseriesPoint3Ds.Add(new SeriesPoint3D(coords[i].Latitude, coords[i].Altitude, coords[i].Longitude));
                    }
                    return lseriesPoint3Ds.ToArray();
                }
                SeriesPoint3D[] Points3DFromCoords2(List<Coord> coords)
                {
                    List<SeriesPoint3D> lseries2Point3Ds = new List<SeriesPoint3D>();

                    for (int i = 0; i < coords.Count; i++)
                    {
                        lseries2Point3Ds.Add(new SeriesPoint3D(coords[i].Latitude, coords[i].Altitude, coords[i].Longitude));
                        lseries2Point3Ds.Add(new SeriesPoint3D(coords[i].Latitude, 0, coords[i].Longitude));
                        lseries2Point3Ds.Add(new SeriesPoint3D(coords[i].Latitude, coords[i].Altitude, coords[i].Longitude));
                    }

                    return lseries2Point3Ds.ToArray();
                }


                void SetDrone(int setDroneIndex, Coord coord)
                {
                    _chart.View3D.MeshModels[setDroneIndex].Position.X = (float)coord.Latitude;
                    _chart.View3D.MeshModels[setDroneIndex].Position.Y = (float)coord.Altitude;
                    _chart.View3D.MeshModels[setDroneIndex].Position.Z = (float)coord.Longitude;
                }
            }

            void AddPointsTraj2(int currentID, int dif, int index)
            {
                //Получаем запись по дрону для работы
                TableUAVRes currentDrone = TablesUAVResList.Where(x => x.Id == currentID).ToList().FirstOrDefault();

                //Получаем траекторию по ID
                List<TableUAVTrajectory> selectCurrentTraj = TableUAVTrajectoryList.Where(x => x.TableUAVResId == currentID).OrderBy(x=> x.Num).ToList();

                if (selectCurrentTraj.Count > 0)
                {
                    int MaxNum = selectCurrentTraj.Max(x => x.Num);

                    //Получаем крайние координаты
                    Coord lastCoords = selectCurrentTraj.Where(x => x.Num == MaxNum).Select(x => x.Coordinates).FirstOrDefault();
                    //получаем крайний Elevation
                    lastCoords.Altitude = (lastCoords.Altitude == 0) ? (selectCurrentTraj.Where(x => x.Num == MaxNum).Select(x => x.Elevation).FirstOrDefault() >= 0) ? selectCurrentTraj.Where(x => x.Num == MaxNum).Select(x => x.Elevation).FirstOrDefault() : 0 : lastCoords.Altitude;

                    //Получаем крайнюю частоту
                    double lastFreq = selectCurrentTraj.Where(x => x.Num == MaxNum).Select(x => x.FrequencyKHz).FirstOrDefault();

                    //Разместить дрон по координатам
                    SetDrone(index + 1, lastCoords);

                    //Разместить туда же аннотацию с хитрым текстом
                    SetСraftyAnnotation(index, lastCoords, lastFreq, currentDrone);


                    PointLineSeries3D newDataSeries = MakeTrajCustom(currentID, ColorPallete(ColorIDPicker[currentID]));

                    PointLineSeries3D newDataSeries2 = MakeTraj2Custom(currentID, Colors.Yellow);

                    //Получить все координаты
                    List<Coord> Coords = selectCurrentTraj.Select(x => x.Coordinates).ToList();
                    //получить все Elevation
                    List<float> Elevations = selectCurrentTraj.Select(x => x.Elevation).ToList();
                    for (int i = 0; i < Coords.Count(); i++)
                    {
                        Coords[i].Altitude = (Coords[i].Altitude == 0) ? (Elevations[i] >= 0) ? Elevations[i] : 0 : Coords[i].Altitude;
                    }

                    //Получить хвост длинной не более TrackLenght
                    List<Coord> trackCoords = Track(Coords);

                    //Заполнить траектории
                    //newDataSeries.Points = Points3DFromCoords(Coords);
                    //newDataSeries2.Points = Points3DFromCoords2(Coords);

                    //Заполнить только хвост траекторий
                    newDataSeries.Points = Points3DFromCoords(trackCoords);
                    newDataSeries2.Points = Points3DFromCoords2(trackCoords);

                    //Добавить точки к существующее траектории
                    int dIndex = index * 2;
                    //_chart.BeginUpdate();
                    _chart.View3D.PointLineSeries3D[dIndex] = newDataSeries;
                    _chart.View3D.PointLineSeries3D[dIndex + 1] = newDataSeries2;
                    //_chart.EndUpdate();
                }

                SeriesPoint3D[] Points3DFromCoords(List<Coord> coords)
                {
                    List<SeriesPoint3D> lseriesPoint3Ds = new List<SeriesPoint3D>();

                    for (int i = 0; i < coords.Count; i++)
                    {
                        lseriesPoint3Ds.Add(new SeriesPoint3D(coords[i].Latitude, coords[i].Altitude, coords[i].Longitude));
                    }
                    return lseriesPoint3Ds.ToArray();
                }
                SeriesPoint3D[] Points3DFromCoords2(List<Coord> coords)
                {
                    List<SeriesPoint3D> lseries2Point3Ds = new List<SeriesPoint3D>();

                    for (int i = 0; i < coords.Count; i++)
                    {
                        lseries2Point3Ds.Add(new SeriesPoint3D(coords[i].Latitude, coords[i].Altitude, coords[i].Longitude));
                        lseries2Point3Ds.Add(new SeriesPoint3D(coords[i].Latitude, 0, coords[i].Longitude));
                        lseries2Point3Ds.Add(new SeriesPoint3D(coords[i].Latitude, coords[i].Altitude, coords[i].Longitude));
                    }

                    return lseries2Point3Ds.ToArray();
                }


                void SetDrone(int setDroneIndex, Coord coord)
                {
                    _chart.View3D.MeshModels[setDroneIndex].Position.X = (float)coord.Latitude;
                    _chart.View3D.MeshModels[setDroneIndex].Position.Y = (float)coord.Altitude;
                    _chart.View3D.MeshModels[setDroneIndex].Position.Z = (float)coord.Longitude;
                }
            }

            PointLineSeries3D MakeTrajCustom(int ID, Color color)
            {
                PointLineSeries3D newDataSeries = new PointLineSeries3D()
                {
                    XAxisBinding = Axis3DBinding.Primary,
                    YAxisBinding = Axis3DBinding.Primary,
                    ZAxisBinding = Axis3DBinding.Primary,
                    MouseInteraction = false,
                    MouseHighlight = MouseOverHighlight.None,
                };

                newDataSeries.PointsVisible = false;
                newDataSeries.LineVisible = true;
                newDataSeries.LineStyle.Width = 0.3f;
                newDataSeries.Title.Text = $"ID: {ID} {Position}";
                newDataSeries.MouseInteraction = false;
                newDataSeries.LineStyle.Color = color;

                return newDataSeries;
            }
            PointLineSeries3D MakeTraj2Custom(int ID, Color color)
            {
                PointLineSeries3D newDataSeries2 = new PointLineSeries3D()
                {
                    XAxisBinding = Axis3DBinding.Primary,
                    YAxisBinding = Axis3DBinding.Primary,
                    ZAxisBinding = Axis3DBinding.Primary,
                    MouseInteraction = false,
                    MouseHighlight = MouseOverHighlight.None,
                };

                newDataSeries2.PointsVisible = false;
                newDataSeries2.LineVisible = true;
                newDataSeries2.LineStyle.Width = 0.1f;
                newDataSeries2.Title.Text = $"ID: {ID} {Trajectory}";
                newDataSeries2.MouseInteraction = false;
                newDataSeries2.LineStyle.Color = color;

                return newDataSeries2;
            }
        }


        Dictionary<string, int> IDcountTrajOldAero = new Dictionary<string, int>();

        Dictionary<string, int> IDcountTrajNewAero = new Dictionary<string, int>();

        private bool AllisnotOKAero(Dictionary<string, int> checkDicOld, Dictionary<string, int> checkDicNew)
        {
            if (checkDicOld.Count != checkDicNew.Count) return true;

            for (int i = 0; i < checkDicOld.Count; i++)
            {
                if (checkDicOld.ElementAt(i).Key != checkDicNew.ElementAt(i).Key) return true;
                if (checkDicOld.ElementAt(i).Value != checkDicNew.ElementAt(i).Value) return true;
            }

            return false;
        }
        private int CheckAero(Dictionary<string, int> checkDicOld, Dictionary<string, int> checkDicNew, int index)
        {
            if (checkDicOld.Count() - 1 < index && index <= checkDicNew.Count() - 1) return 1;

            if (checkDicNew.Count() - 1 < index && index <= checkDicOld.Count() - 1) return 2;

            if (checkDicOld.ElementAt(index).Key != checkDicNew.ElementAt(index).Key) return 3;
            if (checkDicOld.ElementAt(index).Value != checkDicNew.ElementAt(index).Value) return 4;

            return 0;
        }

        public void PlotAero3(List<TableAeroscope> TablesAeroResList, List<TableAeroscopeTrajectory> TableAeroTrajectoryList)
        {
            //Получить ListId
            List<string> ListSN = TablesAeroResList.Select(x => x.SerialNumber).ToList();

            //В любом случае заполняем новый лист аеро что пришло
            for (int i = 0; i < ListSN.Count(); i++)
            {
                //Получить SN для работы
                string currentSN = ListSN[i];

                //Получаем кол-во точек траектории по SN
                int countTrajAero = TableAeroTrajectoryList.Where(x => x.SerialNumber == currentSN).ToList().Count();

                if (countTrajAero > 0)
                {
                    //Добавляем в Словарь ново-пришедших
                    IDcountTrajNewAero.Add(currentSN, countTrajAero);
                }
            }

            //Теперь будет лист аеро уже упорядоченный после словаря
            ListSN = IDcountTrajNewAero.Keys.ToList();

            ColorWorkAero(ListSN);

            ActiveAeroCount = ListSN.Count();

            while (AllisnotOKAero(IDcountTrajOldAero, IDcountTrajNewAero))
            {
                if (IDcountTrajOldAero.Count == 0)
                {
                    //Добавляем всё что есть
                    for (int i = 0; i < IDcountTrajNewAero.Count(); i++)
                    {
                        InsertRecordAero(IDcountTrajNewAero.ElementAt(i).Key, i);
                        IDcountTrajOldAero.Add(IDcountTrajNewAero.ElementAt(i).Key, IDcountTrajNewAero.ElementAt(i).Value);
                    }
                }
                else
                {
                    //суперски сравниваем чё кого пришло и решаем что по жизни

                    //Для начала сравниваем по кол-ву
                    if (IDcountTrajOldAero.Count == IDcountTrajNewAero.Count)
                    {
                        //если кол-во одинаково, может там где добавились точки траектории
                        for (int i = 0; i < IDcountTrajNewAero.Count(); i++)
                        {
                            switch (CheckAero(IDcountTrajOldAero, IDcountTrajNewAero, i))
                            {
                                case 0:
                                    break;
                                case 1:
                                    break;
                                case 2:
                                    break;
                                case 3:
                                    break;
                                case 4:
                                    //AddPointsTrajAero(IDcountTrajNewAero.ElementAt(i).Key, IDcountTrajNewAero.ElementAt(i).Value - IDcountTrajOldAero.ElementAt(i).Value, i);
                                    AddPointsTrajAero2(IDcountTrajNewAero.ElementAt(i).Key, IDcountTrajNewAero.ElementAt(i).Value - IDcountTrajOldAero.ElementAt(i).Value, i);

                                    IDcountTrajOldAero.Remove(IDcountTrajNewAero.ElementAt(i).Key);
                                    if (IDcountTrajOldAero.Count == 0) IDcountTrajOldAero.Clear();
                                    IDcountTrajOldAero.Add(IDcountTrajNewAero.ElementAt(i).Key, IDcountTrajNewAero.ElementAt(i).Value);
                                    break;
                            }
                        }
                    }
                    else
                    {
                        //если кол-во неравно, то может быть больше, а может и меньше
                        if (IDcountTrajOldAero.Count < IDcountTrajNewAero.Count)
                        {
                            for (int i = 0; i < IDcountTrajNewAero.Count(); i++)
                            {
                                switch (CheckAero(IDcountTrajOldAero, IDcountTrajNewAero, i))
                                {
                                    case 0:
                                        break;
                                    case 1:
                                        InsertRecordAero(IDcountTrajNewAero.ElementAt(i).Key, i);
                                        IDcountTrajOldAero.Add(IDcountTrajNewAero.ElementAt(i).Key, IDcountTrajNewAero.ElementAt(i).Value);
                                        break;
                                    case 2:
                                        break;
                                    case 3:
                                        InsertRecordAero(IDcountTrajNewAero.ElementAt(i).Key, i);
                                        IDcountTrajOldAero.Add(IDcountTrajNewAero.ElementAt(i).Key, IDcountTrajNewAero.ElementAt(i).Value);
                                        break;
                                    case 4:
                                        break;
                                }
                            }
                        }
                        if (IDcountTrajOldAero.Count > IDcountTrajNewAero.Count)
                        {
							int localCountTrajOldAero = IDcountTrajOldAero.Count();
                            for (int i = 0, iterations = 0; i < IDcountTrajOldAero.Count(); i++, iterations++)
                            {
                                switch (CheckAero(IDcountTrajOldAero, IDcountTrajNewAero, i))
                                {
                                    case 0:
                                        break;
                                    case 1:
                                        break;
                                    case 2:
                                        //RemoveRecordAero(i);
                                        RemoveRecordAero2(iterations,i);
                                        IDcountTrajOldAero.Remove(IDcountTrajOldAero.ElementAt(i).Key);
                                        if (iterations < localCountTrajOldAero) i--;
                                        if (IDcountTrajOldAero.Count == 0) IDcountTrajOldAero.Clear();
                                        break;
                                    case 3:
                                        RemoveRecordAero(i);
                                        IDcountTrajOldAero.Remove(IDcountTrajOldAero.ElementAt(i).Key);
                                        if (IDcountTrajOldAero.Count == 0) IDcountTrajOldAero.Clear();
                                        break;
                                    case 4:
                                        break;
                                }
                            }
                        }
                    }
                }
            }

            IDcountTrajNewAero.Clear();


            void InsertRecordAero(string currentSN, int index)
            {
                //Получаем запись по дрону для работы
                TableAeroscope currentAero = TablesAeroResList.Where(x => x.SerialNumber == currentSN).ToList().FirstOrDefault();

                //Получаем траекторию по ID
                List<TableAeroscopeTrajectory> selectCurrentTrajAero = TableAeroTrajectoryList.Where(x => x.SerialNumber == currentSN).OrderBy(x=>x.Num).ToList();

                if (selectCurrentTrajAero.Count > 0)
                {
                    int MaxNum = selectCurrentTrajAero.Max(x => x.Num);

                    //Получаем крайние координаты
                    Coord lastCoords = selectCurrentTrajAero.Where(x => x.Num == MaxNum).Select(x => x.Coordinates).FirstOrDefault();

                    //Получаем крайнюю высоту полёта
                    float Elevation = selectCurrentTrajAero.Where(x => x.Num == MaxNum).Select(x => x.Elevation).FirstOrDefault();

                    //Пишем крайнюю высоту в полёта в крайние координаты
                    lastCoords.Altitude = Elevation;

                    //Разместить дрон по координатам
                    SetDroneAero(index + 1, lastCoords);

                    //Разместить туда же аннотацию аеро с хитрым текстом
                    SetСraftyAnnotationAero(index, lastCoords, currentAero);

                    //Сделать траектории
                    PointLineSeries3D newDataSeries = MakeTrajCustom(currentSN, ColorPalleteGrey80(ColorIDPickerAero[currentSN]));
                    PointLineSeries3D newDataSeries2 = MakeTraj2Custom(currentSN, Colors.LightYellow);

                    //Получить все координаты
                    List<Coord> Coords = selectCurrentTrajAero.Select(x => x.Coordinates).ToList();
                    //Получить все Elevation
                    List<float> Elevations = selectCurrentTrajAero.Select(x => x.Elevation).ToList();
                    for (int i = 0; i < Coords.Count(); i++)
                    {
                        Coords[i].Altitude = Elevations[i];
                    }

                    //Получить хвост длинной не более TrackLenght
                    List<Coord> trackCoords = Track(Coords);

                    //Заполнить траектории
                    //newDataSeries.Points = Points3DFromCoords(Coords);
                    //newDataSeries2.Points = Points3DFromCoords2(Coords);

                    //Заполнить только хвост траекторий
                    newDataSeries.Points = Points3DFromCoords(trackCoords);
                    newDataSeries2.Points = Points3DFromCoords2(trackCoords);

                    //Вставить траектории аеро на 3D в нужное место, после UAV
                    int dIndex = IDcountTrajOld.Count * 2 + index * 2;
                    _chart.View3D.PointLineSeries3D.Insert(dIndex, newDataSeries);
                    _chart.View3D.PointLineSeries3D.Insert(dIndex + 1, newDataSeries2);
                }

                SeriesPoint3D[] Points3DFromCoords(List<Coord> coords)
                {
                    List<SeriesPoint3D> lseriesPoint3Ds = new List<SeriesPoint3D>();

                    for (int i = 0; i < coords.Count; i++)
                    {
                        lseriesPoint3Ds.Add(new SeriesPoint3D(coords[i].Latitude, coords[i].Altitude, coords[i].Longitude));
                    }
                    return lseriesPoint3Ds.ToArray();
                }
                SeriesPoint3D[] Points3DFromCoords2(List<Coord> coords)
                {
                    List<SeriesPoint3D> lseries2Point3Ds = new List<SeriesPoint3D>();

                    for (int i = 0; i < coords.Count; i++)
                    {
                        lseries2Point3Ds.Add(new SeriesPoint3D(coords[i].Latitude, coords[i].Altitude, coords[i].Longitude));
                        lseries2Point3Ds.Add(new SeriesPoint3D(coords[i].Latitude, 0, coords[i].Longitude));
                        lseries2Point3Ds.Add(new SeriesPoint3D(coords[i].Latitude, coords[i].Altitude, coords[i].Longitude));
                    }

                    return lseries2Point3Ds.ToArray();
                }
            }

            void RemoveRecordAero(int index)
            {
                //Вернуть Аеродрон на место
                _chart.View3D.MeshModels[1 + _UAVDronesCount + index].Position.X = 0;
                _chart.View3D.MeshModels[1 + _UAVDronesCount + index].Position.Y = 0;
                _chart.View3D.MeshModels[1 + _UAVDronesCount + index].Position.Z = 0;

                //Вернуть Аероаннотацию на место
                _chart.View3D.Annotations[_UAVDronesCount + index].TargetAxisValues.SetValues(0, 0, 0);
                _chart.View3D.Annotations[_UAVDronesCount + index].Text = "";

                //Убрать траекторию, которая идёт после UAV
                int killIndex = IDcountTrajOld.Count * 2 + index * 2;
                _chart.View3D.PointLineSeries3D.RemoveAt(killIndex);
                _chart.View3D.PointLineSeries3D.RemoveAt(killIndex);
            }

			void RemoveRecordAero2(int indexRemoveAero, int indexRemoveTraj)
            {
                //Вернуть Аеродрон на место
                _chart.View3D.MeshModels[1 + _UAVDronesCount + indexRemoveAero].Position.X = 0;
                _chart.View3D.MeshModels[1 + _UAVDronesCount + indexRemoveAero].Position.Y = 0;
                _chart.View3D.MeshModels[1 + _UAVDronesCount + indexRemoveAero].Position.Z = 0;

                //Вернуть Аероаннотацию на место
                _chart.View3D.Annotations[_UAVDronesCount + indexRemoveAero].TargetAxisValues.SetValues(0, 0, 0);
                _chart.View3D.Annotations[_UAVDronesCount + indexRemoveAero].Text = "";

                //Убрать траекторию, которая идёт после UAV
                int killIndex = IDcountTrajOld.Count * 2 + indexRemoveTraj * 2;
                _chart.View3D.PointLineSeries3D.RemoveAt(killIndex);
                _chart.View3D.PointLineSeries3D.RemoveAt(killIndex);
            }
			
            void AddPointsTrajAero(string currentSN, int dif, int index)
            {
                //Получаем запись по дрону для работы
                TableAeroscope currentDroneAero = TablesAeroResList.Where(x => x.SerialNumber == currentSN).ToList().FirstOrDefault();

                //Получаем траекторию по ID
                List<TableAeroscopeTrajectory> selectCurrentTrajAero = TableAeroTrajectoryList.Where(x => x.SerialNumber == currentSN).ToList();

                if (selectCurrentTrajAero.Count > 0)
                {
                    int MaxNum = selectCurrentTrajAero.Max(x => x.Num);

                    //Получаем крайние координаты
                    Coord lastCoords = selectCurrentTrajAero.Where(x => x.Num == MaxNum).Select(x => x.Coordinates).FirstOrDefault();

                    //Разместить дрон по координатам
                    SetDroneAero(index + 1, lastCoords);

                    //Разместить туда же аннотацию аеро с хитрым текстом
                    SetСraftyAnnotationAero(index, lastCoords, currentDroneAero);

                    //Лист для крайних координат
                    List<Coord> Coords = new List<Coord>();

                    //Заполнить его крайними координатами
                    for (int w = selectCurrentTrajAero.Count() - dif; w < selectCurrentTrajAero.Count(); w++)
                    {
                        Coords.Add(selectCurrentTrajAero[w].Coordinates);
                    }

                    //Заполнить точки в нужном виде
                    var Points = Points3DFromCoords(Coords);
                    var Points2 = Points3DFromCoords2(Coords);

                    //Добавить точки к существующее траектории после UAV
                    int dIndex = IDcountTrajOld.Count * 2;
                    _chart.View3D.PointLineSeries3D[dIndex + index].AddPoints(Points, false);
                    _chart.View3D.PointLineSeries3D[dIndex + index + 1].AddPoints(Points2, false);
                }

                SeriesPoint3D[] Points3DFromCoords(List<Coord> coords)
                {
                    List<SeriesPoint3D> lseriesPoint3Ds = new List<SeriesPoint3D>();

                    for (int i = 0; i < coords.Count; i++)
                    {
                        lseriesPoint3Ds.Add(new SeriesPoint3D(coords[i].Latitude, coords[i].Altitude, coords[i].Longitude));
                    }
                    return lseriesPoint3Ds.ToArray();
                }
                SeriesPoint3D[] Points3DFromCoords2(List<Coord> coords)
                {
                    List<SeriesPoint3D> lseries2Point3Ds = new List<SeriesPoint3D>();

                    for (int i = 0; i < coords.Count; i++)
                    {
                        lseries2Point3Ds.Add(new SeriesPoint3D(coords[i].Latitude, coords[i].Altitude, coords[i].Longitude));
                        lseries2Point3Ds.Add(new SeriesPoint3D(coords[i].Latitude, 0, coords[i].Longitude));
                        lseries2Point3Ds.Add(new SeriesPoint3D(coords[i].Latitude, coords[i].Altitude, coords[i].Longitude));
                    }

                    return lseries2Point3Ds.ToArray();
                }
            }

            void AddPointsTrajAero2(string currentSN, int dif, int index)
            {
                //Получаем запись по дрону для работы
                TableAeroscope currentDroneAero = TablesAeroResList.Where(x => x.SerialNumber == currentSN).ToList().FirstOrDefault();

                //Получаем траекторию по ID
                List<TableAeroscopeTrajectory> selectCurrentTrajAero = TableAeroTrajectoryList.Where(x => x.SerialNumber == currentSN).OrderBy(x=> x.Num).ToList();

                if (selectCurrentTrajAero.Count > 0)
                {
                    int MaxNum = selectCurrentTrajAero.Max(x => x.Num);

                    //Получаем крайние координаты
                    Coord lastCoords = selectCurrentTrajAero.Where(x => x.Num == MaxNum).Select(x => x.Coordinates).FirstOrDefault();

                    //Получаем крайнюю высоту полёта
                    float Elevation = selectCurrentTrajAero.Where(x => x.Num == MaxNum).Select(x => x.Elevation).FirstOrDefault();

                    //Пишем крайнюю высоту в полёта в крайние координаты
                    lastCoords.Altitude = Elevation;

                    //Разместить дрон по координатам
                    SetDroneAero(index + 1, lastCoords);

                    //Разместить туда же аннотацию аеро с хитрым текстом
                    SetСraftyAnnotationAero(index, lastCoords, currentDroneAero);

                    //Сделать траектории
                    PointLineSeries3D newDataSeries = MakeTrajCustom(currentSN, ColorPalleteGrey80(ColorIDPickerAero[currentSN]));
                    PointLineSeries3D newDataSeries2 = MakeTraj2Custom(currentSN, Colors.LightYellow);

                    //Получить все координаты
                    List<Coord> Coords = selectCurrentTrajAero.Select(x => x.Coordinates).ToList();
                    //Получить все Elevation
                    List<float> Elevations = selectCurrentTrajAero.Select(x => x.Elevation).ToList();
                    for (int i = 0; i < Coords.Count(); i++)
                    {
                        Coords[i].Altitude = Elevations[i];
                    }

                    //Получить хвост длинной не более TrackLenght
                    List<Coord> trackCoords = Track(Coords);

                    //Заполнить траектории
                    //newDataSeries.Points = Points3DFromCoords(Coords);
                    //newDataSeries2.Points = Points3DFromCoords2(Coords);

                    //Заполнить только хвост траекторий
                    newDataSeries.Points = Points3DFromCoords(trackCoords);
                    newDataSeries2.Points = Points3DFromCoords2(trackCoords);

                    //Добавить точки к существующее траектории после UAV
                    int dIndex = IDcountTrajOld.Count * 2 + index * 2;
                    _chart.View3D.PointLineSeries3D[dIndex] = newDataSeries;
                    _chart.View3D.PointLineSeries3D[dIndex + 1] = newDataSeries2;
                }

                SeriesPoint3D[] Points3DFromCoords(List<Coord> coords)
                {
                    List<SeriesPoint3D> lseriesPoint3Ds = new List<SeriesPoint3D>();

                    for (int i = 0; i < coords.Count; i++)
                    {
                        lseriesPoint3Ds.Add(new SeriesPoint3D(coords[i].Latitude, coords[i].Altitude, coords[i].Longitude));
                    }
                    return lseriesPoint3Ds.ToArray();
                }
                SeriesPoint3D[] Points3DFromCoords2(List<Coord> coords)
                {
                    List<SeriesPoint3D> lseries2Point3Ds = new List<SeriesPoint3D>();

                    for (int i = 0; i < coords.Count; i++)
                    {
                        lseries2Point3Ds.Add(new SeriesPoint3D(coords[i].Latitude, coords[i].Altitude, coords[i].Longitude));
                        lseries2Point3Ds.Add(new SeriesPoint3D(coords[i].Latitude, 0, coords[i].Longitude));
                        lseries2Point3Ds.Add(new SeriesPoint3D(coords[i].Latitude, coords[i].Altitude, coords[i].Longitude));
                    }

                    return lseries2Point3Ds.ToArray();
                }
            }

            PointLineSeries3D MakeTrajCustom(string SN, Color color)
            {
                PointLineSeries3D newDataSeries = new PointLineSeries3D()
                {
                    XAxisBinding = Axis3DBinding.Primary,
                    YAxisBinding = Axis3DBinding.Primary,
                    ZAxisBinding = Axis3DBinding.Primary,
                    MouseInteraction = false,
                    MouseHighlight = MouseOverHighlight.None,
                };

                newDataSeries.PointsVisible = false;
                newDataSeries.LineVisible = true;
                newDataSeries.LineStyle.Width = 0.3f;
                newDataSeries.Title.Text = $"SN: {SN} {Position}";
                newDataSeries.MouseInteraction = false;
                newDataSeries.LineStyle.Color = color;

                return newDataSeries;
            }
            PointLineSeries3D MakeTraj2Custom(string SN, Color color)
            {
                PointLineSeries3D newDataSeries2 = new PointLineSeries3D()
                {
                    XAxisBinding = Axis3DBinding.Primary,
                    YAxisBinding = Axis3DBinding.Primary,
                    ZAxisBinding = Axis3DBinding.Primary,
                    MouseInteraction = false,
                    MouseHighlight = MouseOverHighlight.None,
                };

                newDataSeries2.PointsVisible = false;
                newDataSeries2.LineVisible = true;
                newDataSeries2.LineStyle.Width = 0.1f;
                newDataSeries2.Title.Text = $"SN: {SN} {Trajectory}";
                newDataSeries2.MouseInteraction = false;
                newDataSeries2.LineStyle.Color = color;

                return newDataSeries2;
            }

            void SetDroneAero(int aeroIndex, Coord coord)
            {
                _chart.View3D.MeshModels[aeroIndex + UAVDronesCount].Position.X = (float)coord.Latitude;
                _chart.View3D.MeshModels[aeroIndex + UAVDronesCount].Position.Y = (float)coord.Altitude;
                _chart.View3D.MeshModels[aeroIndex + UAVDronesCount].Position.Z = (float)coord.Longitude;
            }
        }


        public void PlotAero(List<TableAeroscope> TablesAeroResList, List<TableAeroscopeTrajectory> TableAeroTrajectoryList)
        {
            ////Очистить траектории
            //_chart.View3D.PointLineSeries3D.Clear();

            //Получить ListId
            List<string> ListSN = TablesAeroResList.Select(x => x.SerialNumber).ToList();

            ColorWorkAero(ListSN);

            //Вернуть почти всё как было
            Default(0, ListSN.Count());

            ActiveAeroCount = ListSN.Count();

            lAeroPLS.Clear();

            for (int i = 0; i < ListSN.Count(); i++)
            {
                //Получить координаты дрона из траекторий

                //Получить ID для работы
                string currentSN = ListSN[i];

                //Получаем запись по дрону для работы
                TableAeroscope currentDrone = TablesAeroResList.Where(x => x.SerialNumber == currentSN).ToList().FirstOrDefault();

                //Получаем траекторию по ID
                List<TableAeroscopeTrajectory> selectCurrentTraj = TableAeroTrajectoryList.Where(x => x.SerialNumber == currentSN).ToList();

                if (selectCurrentTraj.Count > 0)
                {
                    int MaxNum = selectCurrentTraj.Max(x => x.Num);

                    //Получаем крайние координаты
                    Coord lastCoords = selectCurrentTraj.Where(x => x.Num == MaxNum).Select(x => x.Coordinates).FirstOrDefault();

                    //Разместить дрон по координатам
                    SetDroneAero(i + 1, lastCoords);

                    //Разместить туда же аннотацию с хитрым текстом
                    SetСraftyAnnotationAero(i, lastCoords, currentDrone);

                    //Сделать траектории
                    PointLineSeries3D newDataSeries = MakeTrajCustom(currentSN, ColorPallete(ColorIDPickerAero[currentSN]));

                    PointLineSeries3D newDataSeries2 = MakeTraj2Custom(currentSN);

                    //Получить все координаты
                    List<Coord> Coords = selectCurrentTraj.Select(x => x.Coordinates).ToList();

                    //Заполнить траектории
                    newDataSeries.Points = Points3DFromCoords(Coords);
                    newDataSeries2.Points = Points3DFromCoords2(Coords);

                    //Добавить траеториии на 3D
                    //_chart.View3D.PointLineSeries3D.Add(newDataSeries);
                    //_chart.View3D.PointLineSeries3D.Add(newDataSeries2);

                    //Вставить АероТраектории куда нужно, но не факт
                    //_chart.View3D.PointLineSeries3D.Insert(i + UAVDronesCount, newDataSeries);
                    //_chart.View3D.PointLineSeries3D.Insert(i + 1 + UAVDronesCount, newDataSeries2);

                    //А если попробовать через лист
                    lAeroPLS.Add(newDataSeries);
                    lAeroPLS.Add(newDataSeries2);
                }
            }
            _chart.View3D.PointLineSeries3D.AddRange(lUAVPLS);
            _chart.View3D.PointLineSeries3D.AddRange(lAeroPLS);

            SeriesPoint3D[] Points3DFromCoords(List<Coord> coords)
            {
                List<SeriesPoint3D> lseriesPoint3Ds = new List<SeriesPoint3D>();

                for (int i = 0; i < coords.Count; i++)
                {
                    lseriesPoint3Ds.Add(new SeriesPoint3D(coords[i].Latitude, coords[i].Altitude, coords[i].Longitude));
                }
                return lseriesPoint3Ds.ToArray();
            }
            SeriesPoint3D[] Points3DFromCoords2(List<Coord> coords)
            {
                List<SeriesPoint3D> lseries2Point3Ds = new List<SeriesPoint3D>();

                for (int i = 0; i < coords.Count; i++)
                {
                    lseries2Point3Ds.Add(new SeriesPoint3D(coords[i].Latitude, coords[i].Altitude, coords[i].Longitude));
                    lseries2Point3Ds.Add(new SeriesPoint3D(coords[i].Latitude, 0, coords[i].Longitude));
                    lseries2Point3Ds.Add(new SeriesPoint3D(coords[i].Latitude, coords[i].Altitude, coords[i].Longitude));
                }

                return lseries2Point3Ds.ToArray();
            }

            PointLineSeries3D MakeTrajCustom(string SN, Color color)
            {
                PointLineSeries3D newDataSeries = new PointLineSeries3D()
                {
                    XAxisBinding = Axis3DBinding.Primary,
                    YAxisBinding = Axis3DBinding.Primary,
                    ZAxisBinding = Axis3DBinding.Primary,
                    MouseInteraction = false,
                    MouseHighlight = MouseOverHighlight.None,
                };

                newDataSeries.PointsVisible = false;
                newDataSeries.LineVisible = true;
                newDataSeries.LineStyle.Width = 0.3f;
                newDataSeries.Title.Text = $"SN: {SN} Position";
                newDataSeries.MouseInteraction = false;
                newDataSeries.LineStyle.Color = color;

                return newDataSeries;
            }
            PointLineSeries3D MakeTraj2Custom(string SN)
            {
                PointLineSeries3D newDataSeries2 = new PointLineSeries3D()
                {
                    XAxisBinding = Axis3DBinding.Primary,
                    YAxisBinding = Axis3DBinding.Primary,
                    ZAxisBinding = Axis3DBinding.Primary,
                    MouseInteraction = false,
                    MouseHighlight = MouseOverHighlight.None,
                };

                newDataSeries2.PointsVisible = false;
                newDataSeries2.LineVisible = true;
                newDataSeries2.LineStyle.Width = 0.1f;
                newDataSeries2.Title.Text = $"SN: {SN} Trajectory";
                newDataSeries2.MouseInteraction = false;
                newDataSeries2.LineStyle.Color = System.Windows.Media.Color.FromRgb(255, 255, 0);

                return newDataSeries2;
            }
            PointLineSeries3D MakeTraj2Color(Color color)
            {
                PointLineSeries3D newDataSeries2 = new PointLineSeries3D()
                {
                    XAxisBinding = Axis3DBinding.Primary,
                    YAxisBinding = Axis3DBinding.Primary,
                    ZAxisBinding = Axis3DBinding.Primary,
                    MouseInteraction = false,
                    MouseHighlight = MouseOverHighlight.None,
                };

                newDataSeries2.PointsVisible = false;
                newDataSeries2.LineVisible = true;
                newDataSeries2.LineStyle.Width = 0.1f;
                newDataSeries2.Title.Text = "Trajectory";
                newDataSeries2.MouseInteraction = false;
                newDataSeries2.LineStyle.Color = color;

                return newDataSeries2;
            }

            void SetDroneAero(int index, Coord coord)
            {
                _chart.View3D.MeshModels[index + UAVDronesCount].Position.X = (float)coord.Latitude;
                _chart.View3D.MeshModels[index + UAVDronesCount].Position.Y = (float)coord.Altitude;
                _chart.View3D.MeshModels[index + UAVDronesCount].Position.Z = (float)coord.Longitude;
            }

        }

        private int _UAVDronesCount = 10;
        public int UAVDronesCount
        {
            get { return _UAVDronesCount; }
            set
            {
                if (_UAVDronesCount != value)
                {
                    _UAVDronesCount = value;
                }
            }
        }

        private void InitUAVDrones()
        {
            string path = System.Reflection.Assembly.GetEntryAssembly().Location;
            var lastindex = path.LastIndexOf("\\");
            path = path.Substring(0, lastindex);
            path = path + "\\Resources\\Drones";

            for (int i = 0; i < UAVDronesCount; i++)
            {
                string pathToFile = path + "\\Drone0" + ".obj";
                var newDroneModel = new MeshModel(_chart.View3D, Axis3DBinding.Primary, Axis3DBinding.Primary, Axis3DBinding.Primary);

                newDroneModel.LoadFromFile(pathToFile);

                newDroneModel.Position.X = 0;
                newDroneModel.Position.Y = 0;
                newDroneModel.Position.Z = 0;

                newDroneModel.MouseInteraction = false;

                newDroneModel.Rotation.SetValues(0, 0, 0);

                newDroneModel.Size.SetValues(0.02f, 0.02f, 0.02f);

                _chart.View3D.MeshModels.Add(newDroneModel);

                AddAnnotation();

            }
        }

        private int _AeroDronesCount = 10;
        public int AeroDronesCount
        {
            get { return _AeroDronesCount; }
            set
            {
                if (_AeroDronesCount != value)
                {
                    _AeroDronesCount = value;
                }
            }
        }

        private void InitAeroDrones()
        {
            string path = System.Reflection.Assembly.GetEntryAssembly().Location;
            var lastindex = path.LastIndexOf("\\");
            path = path.Substring(0, lastindex);
            path = path + "\\Resources\\Drones";

            for (int i = 0; i < AeroDronesCount; i++)
            {
                string pathToFile = path + "\\Drone1" + ".obj";
                var newDroneModel = new MeshModel(_chart.View3D, Axis3DBinding.Primary, Axis3DBinding.Primary, Axis3DBinding.Primary);

                newDroneModel.LoadFromFile(pathToFile);

                newDroneModel.Position.X = 0;
                newDroneModel.Position.Y = 0;
                newDroneModel.Position.Z = 0;

                newDroneModel.MouseInteraction = false;

                newDroneModel.Rotation.SetValues(0, 0, 0);

                newDroneModel.Size.SetValues(0.02f, 0.02f, 0.02f);

                _chart.View3D.MeshModels.Add(newDroneModel);

                AddAnnotation();

            }
        }

        private void SetСraftyAnnotation(int index, Coord coord, double Freq, TableUAVRes currentDrone)
        {
            _chart.View3D.Annotations[index].TargetAxisValues.SetValues(coord.Latitude, coord.Altitude, coord.Longitude);
            //_chart.View3D.Annotations[0].Text = "DJI" + "\nX: " + meshModel.Position.X.ToString("F6") + "\nY: " + meshModel.Position.Y.ToString("F0") + "\nZ: " + meshModel.Position.Z.ToString("F6");
            //_chart.View3D.Annotations[index].Text = settings3D.DroneModel + "\nF = " + settings3D.DroneFreq + "\nH = " + meshModel.Position.Y.ToString("F0") + " m";
            _chart.View3D.Annotations[index].Text = $"ID: {currentDrone.Id}\nType: {currentDrone.Type.ToString()}\nF = {Freq.ToString("F0")} kHz\nH = {coord.Altitude.ToString("F0")} m";
        }

        private void SetСraftyAnnotationAero(int index, Coord coord, TableAeroscope currentDrone)
        {
            _chart.View3D.Annotations[index + UAVDronesCount].TargetAxisValues.SetValues(coord.Latitude, coord.Altitude, coord.Longitude);
            _chart.View3D.Annotations[index + UAVDronesCount].Text =
                $"SN: {currentDrone.SerialNumber}\n" +
                $"Type: Aero\n" +
                $"H = {coord.Altitude.ToString("F0")} m";
        }


        public void Default(int countUAV = 0, int countAero = 0)
        {
            //Очистить траектории
            //if (countUAV == 0 && countAero == 0)
            {
                _chart.View3D.PointLineSeries3D.Clear();
            }

            //Очистить сразу с конца countAero икс два
            //if (countAero != 0)
            //{
            //    _chart.View3D.PointLineSeries3D.RemoveRange(countUAV * 2, countUAV * 2 + countAero * 2);
            //}

            //Очистить первые countUAV икс два
            //if (countUAV != 0)
            //{
            //    _chart.View3D.PointLineSeries3D.RemoveRange(0, countUAV * 2);
            //}


            if (countUAV == -1)
            {
                _chart.View3D.PointLineSeries3D.AddRange(lAeroPLS);
                lUAVPLS.Clear();

                countUAV = 0;

                //Вернуть все UAVдроны на место
                for (int i = 1 + countUAV; i < UAVDronesCount + 1; i++)
                {
                    _chart.View3D.MeshModels[i].Position.X = 0;
                    _chart.View3D.MeshModels[i].Position.Y = 0;
                    _chart.View3D.MeshModels[i].Position.Z = 0;
                }

                //Вернуть UAVаннотации на место
                for (int i = 0 + countUAV; i < UAVDronesCount; i++)
                {
                    _chart.View3D.Annotations[i].TargetAxisValues.SetValues(0, 0, 0);
                    _chart.View3D.Annotations[i].Text = "";
                }
            }

            if (countAero == -1)
            {
                _chart.View3D.PointLineSeries3D.AddRange(lUAVPLS);
                lAeroPLS.Clear();

                countAero = 0;
                //Вернуть все Aeroдроны на место
                for (int i = 1 + UAVDronesCount + countAero; i < UAVDronesCount + AeroDronesCount + 1; i++)
                {
                    _chart.View3D.MeshModels[i].Position.X = 0;
                    _chart.View3D.MeshModels[i].Position.Y = 0;
                    _chart.View3D.MeshModels[i].Position.Z = 0;
                }

                //Вернуть Aeroаннотации на место
                for (int i = 0 + UAVDronesCount + countAero; i < UAVDronesCount + AeroDronesCount; i++)
                {
                    _chart.View3D.Annotations[i].TargetAxisValues.SetValues(0, 0, 0);
                    _chart.View3D.Annotations[i].Text = "";
                }
            }


            if (countUAV != 0)
            {
                //Вернуть все UAVдроны на место
                for (int i = 1 + countUAV; i < UAVDronesCount + 1; i++)
                {
                    _chart.View3D.MeshModels[i].Position.X = 0;
                    _chart.View3D.MeshModels[i].Position.Y = 0;
                    _chart.View3D.MeshModels[i].Position.Z = 0;
                }

                //Вернуть UAVаннотации на место
                for (int i = 0 + countUAV; i < UAVDronesCount; i++)
                {
                    _chart.View3D.Annotations[i].TargetAxisValues.SetValues(0, 0, 0);
                    _chart.View3D.Annotations[i].Text = "";
                }
            }

            if (countAero != 0)
            {
                //Вернуть все Aeroдроны на место
                for (int i = 1 + UAVDronesCount + countAero; i < UAVDronesCount + AeroDronesCount + 1; i++)
                {
                    _chart.View3D.MeshModels[i].Position.X = 0;
                    _chart.View3D.MeshModels[i].Position.Y = 0;
                    _chart.View3D.MeshModels[i].Position.Z = 0;
                }

                //Вернуть Aeroаннотации на место
                for (int i = 0 + UAVDronesCount + countAero; i < UAVDronesCount + AeroDronesCount; i++)
                {
                    _chart.View3D.Annotations[i].TargetAxisValues.SetValues(0, 0, 0);
                    _chart.View3D.Annotations[i].Text = "";
                }
            }
        }


    }

}
