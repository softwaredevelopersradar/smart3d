﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.Serialization;

namespace smart3D
{
    public partial class MainWindow
    {
        public Settings3D YamlLoad()
        {
            string text = "";
            try
            {
                using (StreamReader sr = new StreamReader("Settings3D.yaml", System.Text.Encoding.Default))
                {
                    text = sr.ReadToEnd();
                    sr.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            var deserializer = new DeserializerBuilder().Build();

            var localProperties = new Settings3D();
            try
            {
                localProperties = deserializer.Deserialize<Settings3D>(text);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            if (localProperties == null)
            {
                localProperties = new Settings3D();
                YamlSave(localProperties);
            }
            return localProperties;
        }

        public T YamlLoad<T>(string NameDotYaml) where T : new()
        {
            string text = "";
            try
            {
                using (StreamReader sr = new StreamReader(NameDotYaml, System.Text.Encoding.Default))
                {
                    text = sr.ReadToEnd();
                    sr.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            var deserializer = new DeserializerBuilder().Build();

            var t = new T();
            try
            {
                t = deserializer.Deserialize<T>(text);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            //if (t == null)
            //{
            //    t = new T();
            //    YamlSave(t, NameDotYaml);
            //}
            return t;
        }

        public void YamlSave(Settings3D settings3D)
        {
            try
            {
                var serializer = new SerializerBuilder().EmitDefaults().Build();
                var yaml = serializer.Serialize(settings3D);

                using (StreamWriter sw = new StreamWriter("Settings3D.yaml", false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(yaml);
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void YamlSave<T>(T t, string NameDotYaml) where T : new()
        {
            try
            {
                var serializer = new SerializerBuilder().EmitDefaults().Build();
                var yaml = serializer.Serialize(t);

                using (StreamWriter sw = new StreamWriter(NameDotYaml, false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(yaml);
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }

    public enum Mode3D : byte
    {
        Demo,
        Work
    }

    public class Settings3D
    {
        public Settings3D()
        {
            Mode = Mode3D.Demo;
            UDPmyIP = "127.0.0.1";
            UDPmyPort = 15003;
            UDPremoteIP = "127.0.0.1";
            UDPremotePort = 15002;
            TimeCoef = 0.1f;
            TailCount = 60;
            DroneModel = "DJI Phantom3";
            DroneFreq = "2412 MHz";
        }

        public Mode3D Mode { get; set; }
        public string UDPmyIP { get; set; }
        public int UDPmyPort { get; set; }
        public string UDPremoteIP { get; set; }
        public int UDPremotePort { get; set; }
        public float TimeCoef { get; set; }
        public int TailCount { get; set; }
        public string DroneModel { get; set; }
        public string DroneFreq { get; set; }
    }

}