﻿using Arction.Wpf.SemibindableCharting;
using Arction.Wpf.SemibindableCharting.Annotations;
using Arction.Wpf.SemibindableCharting.Series3D;
using Arction.Wpf.SemibindableCharting.Views.View3D;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace smart3D
{
    public partial class MainWindow : Window
    {
        private void InitializeDeploymentKey()
        {
            string deploymentKey = "lgCAABW2ij + vBNQBJABVcGRhdGVhYmxlVGlsbD0yMDE5LTA2LTE1I1JldmlzaW9uPTACgD + BCRGnn7c6dwaDiJovCk5g5nFwvJ + G60VSdCrAJ + jphM8J45NmxWE1ZpK41lW1wuI4Hz3bPIpT7aP9zZdtXrb4379WlHowJblnk8jEGJQcnWUlcFnJSl6osPYvkxfq / B0dVcthh7ezOUzf1uXfOcEJ377 / 4rwUTR0VbNTCK601EN6 / ciGJmHars325FPaj3wXDAUIehxEfwiN7aa7HcXH6RqwOF6WcD8voXTdQEsraNaTYbIqSMErzg6HFsaY5cW4IkG6TJ3iBFzXCVfvPRZDxVYMuM + Q5vztCEz5k + Luaxs + S + OQD3ELg8 + y7a / Dv0OhSQkqMDrR / o7mjauDnZVt5VRwtvDYm6kDNOsNL38Ry / tAsPPY26Ff3PDl1ItpFWZCzNS / xfDEjpmcnJOW7hmZi6X17LM66whLUTiCWjj81lpDi + VhBSMI3a2I7jmiFONUKhtD91yrOyHrCWObCdWq + F5H4gjsoP0ffEKcx658a3ZF8VhtL8d9 + B0YtxFPNBQs =";
            Arction.Wpf.SemibindableCharting.LightningChartUltimate.SetDeploymentKey(deploymentKey);
        }

        private static int tailCount1 = 60;
        private static int tailCount2 = tailCount1 * 3;

        private Settings3D settings3D;

        public MainWindow()
        {
            InitializeDeploymentKey();
            InitializeComponent();

            InitCoordsLightsAxes();

            viewPointEditor1.Chart = _chart;

            Init3DDataSeries();

            LoadFromYaml();

            ApplyMode();

            myGrid.ColumnDefinitions[1].Width = (tbVPE.IsChecked.Value) ? new GridLength(1, GridUnitType.Auto) : new GridLength(0);

            InitZones();

        }

        Zone RedZone;
        Zone YellowZone;

        private void InitZones()
        {
            (double Latitude, double Longitude) CenterPoint = (53.931672, 27.636670);

            (double Latitude, double Longitude) CenterPoint2 = (53.931272, 27.636970);

            double standartX = 484;
            double standartZ = 478;

            double coef3 = 3.0d;

            var color1 = (System.Windows.Media.Color.FromArgb(225,255,0,0));

            RedZone = AddZone(color1, CenterPoint2, standartX / coef3, standartZ / coef3);

            double coef2 = 2.0d;

            var color2 = (System.Windows.Media.Color.FromArgb(225, 255, 255, 0));

            YellowZone = AddZone(color2, CenterPoint, standartX / coef2, standartZ / coef2);
        }


        List<MeshModel> meshModels = new List<MeshModel>();

        private void InitDrones()
        {
            string path = System.Reflection.Assembly.GetEntryAssembly().Location;
            var lastindex = path.LastIndexOf("\\");
            path = path.Substring(0, lastindex);
            path = path + "\\Resources\\Drones";

            var AllDroneFiles = Directory.GetFiles(path);

            for (int i = 0; i < AllDroneFiles.Count(); i++)
            {
                string pathToFile = path + "\\Drone" + i + ".obj";
                FileInfo tempFileInfo = new FileInfo(pathToFile);
                if (tempFileInfo.Exists)
                {
                    var newDroneModel = new MeshModel(_chart.View3D, Axis3DBinding.Primary, Axis3DBinding.Primary, Axis3DBinding.Primary);

                    newDroneModel.LoadFromFile(pathToFile);

                    newDroneModel.Position.X = 0;
                    newDroneModel.Position.Y = 0;
                    newDroneModel.Position.Z = 0;

                    newDroneModel.MouseInteraction = false;

                    newDroneModel.Rotation.SetValues(0, 0, 0);

                    //newDroneModel.Size.SetValues(0.02f, 0.02f, 0.02f);
                    newDroneModel.Size.SetValues(0.01f, 0.01f, 0.01f);

                    meshModels.Add(newDroneModel);
                }
            }

            _chart.View3D.MeshModels.Add(meshModels[0]);
            AddAnnotation();
            SetAnnotation(_chart.View3D.MeshModels[1]);
        }

        private void ApplyMode()
        {
            InitLandscape();
            InitDrones();

            switch (settings3D.Mode)
            {
                case Mode3D.Demo:

                    LoadAeroScopeEmulator("Samples");
                    Demo();

                    break;

                case Mode3D.Work:

                    InitUDPReceiver(settings3D);

                    break;

            }
        }

        private void LoadFromYaml()
        {
            settings3D = YamlLoad();
            tailCount1 = settings3D.TailCount;
        }

        private void InitLandscape()
        {
            //Load Landscape
            {
                //Disable rendering, strongly recommended before updating chart properties
                _chart.BeginUpdate();

                MeshModel model = new MeshModel(
                _chart.View3D,
                Axis3DBinding.Primary,
                Axis3DBinding.Primary,
                Axis3DBinding.Primary);

                string temp = System.Reflection.Assembly.GetEntryAssembly().Location;
                var lastindex = temp.LastIndexOf("\\");
                temp = temp.Substring(0, lastindex);
                temp = temp + "\\Resources\\Landscape\\3d_metashape_1M.obj";
                model.LoadFromFile(temp);

                model.Position.X = (float)_chart.View3D.XAxisPrimary3D.Minimum + (float)(_chart.View3D.XAxisPrimary3D.Maximum - _chart.View3D.XAxisPrimary3D.Minimum) * 0.5f;
                model.Position.Y = 0;
                model.Position.Z = (float)_chart.View3D.ZAxisPrimary3D.Minimum + (float)(_chart.View3D.ZAxisPrimary3D.Maximum - _chart.View3D.ZAxisPrimary3D.Minimum) * 0.5f;

                //size
                model.Size.SetValues(0.25f, 0.25f, 0.25f);

                //rotation
                model.Rotation.SetValues(0, -90, 0);


                model.MouseInteraction = false;
                _chart.View3D.MeshModels.Add(model);

                //Allow chart rendering
                _chart.EndUpdate();
            }

            //visibility
            AxisVisibility(false);
        }

        private void Init3DDataSeries()
        {
            dataSeries.PointsVisible = false;
            dataSeries.LineVisible = true;
            //dataSeries.LineStyle.Width = 0.3f;
            dataSeries.LineStyle.Width = 0.2f;
            //dataSeries.LineStyle.Width = 0.1f;
            dataSeries.Title.Text = "Position";
            dataSeries.MouseInteraction = false;
            dataSeries.LineStyle.Color = System.Windows.Media.Color.FromRgb(255, 0, 0);

            dataSeries2.PointsVisible = false;
            dataSeries2.LineVisible = true;
            //dataSeries2.LineStyle.Width = 0.3f;
            dataSeries2.Title.Text = "Trajectory";
            dataSeries2.MouseInteraction = false;
            dataSeries2.LineStyle.Color = System.Windows.Media.Color.FromRgb(255, 255, 0);
        }

        private MapCoordinate m_mapCoordCorner;
        private MapCoordinate m_mapCoordMaxLat;
        private MapCoordinate m_mapCoordMaxLon;


        private void InitMapCoords1M()
        {
            m_mapCoordCorner = new MapCoordinate(53.930210, 27.632743);
            m_mapCoordMaxLat = new MapCoordinate(53.934570, 27.632773);
            m_mapCoordMaxLon = new MapCoordinate(53.930162, 27.640043);
        }

        private void InitLights()
        {
            var lights = View3D.CreateDefaultLights();
            {
                _chart.View3D.SetPredefinedLightingScheme(LightingScheme.DiscoCMY);

                _chart.View3D.Lights[0].AmbientColor = System.Windows.Media.Color.FromArgb(255, 0, 0, 0);
                _chart.View3D.Lights[0].AttenuationConstant = 0.5;
                _chart.View3D.Lights[0].AttenuationLinear = 0.1;
                _chart.View3D.Lights[0].AttenuationQuadratic = 0.05;
                _chart.View3D.Lights[0].DiffuseColor = System.Windows.Media.Color.FromArgb(255, 255, 255, 255);
                _chart.View3D.Lights[0].SpecularColor = System.Windows.Media.Color.FromArgb(255, 128, 128, 128);

                _chart.View3D.Lights[0].Type = LightType.Directional;
                _chart.View3D.Lights[0].Location.SetValues(-30, 50, -30);
                _chart.View3D.Lights[0].Target.SetValues(0, 0, 0);


                _chart.View3D.Lights[1].AmbientColor = System.Windows.Media.Color.FromArgb(255, 0, 0, 0);
                _chart.View3D.Lights[1].DiffuseColor = System.Windows.Media.Color.FromArgb(255, 255, 255, 255);
                _chart.View3D.Lights[1].SpecularColor = System.Windows.Media.Color.FromArgb(255, 128, 128, 128);

                _chart.View3D.Lights[2].AmbientColor = System.Windows.Media.Color.FromArgb(255, 0, 0, 0);
                _chart.View3D.Lights[2].DiffuseColor = System.Windows.Media.Color.FromArgb(255, 255, 255, 255);
                _chart.View3D.Lights[2].SpecularColor = System.Windows.Media.Color.FromArgb(255, 128, 128, 128);
            }
        }

        private void InitAxes()
        {
            //X axis shows latitude 
            {
                _chart.View3D.XAxisPrimary3D.SetRange(m_mapCoordCorner.Latitude, m_mapCoordMaxLat.Latitude);

                _chart.View3D.XAxisPrimary3D.Reversed = true;

                _chart.View3D.XAxisPrimary3D.ValueType = AxisValueType.MapCoordsDegNESW;

                _chart.View3D.XAxisPrimary3D.Title.Text = "Latitude";
                _chart.View3D.XAxisPrimary3D.LabelsColor = Colors.White;
                _chart.View3D.XAxisPrimary3D.Title.Color = Colors.LightCyan;
                _chart.View3D.XAxisPrimary3D.LabelTicksGap = 20;

                _chart.View3D.XAxisPrimary3D.MouseDragSnapToDiv = false;
                _chart.View3D.XAxisPrimary3D.ScaleNibs = new Arction.Wpf.SemibindableCharting.Axes.AxisDragNib3D();

                _chart.View3D.XAxisPrimary3D.MouseInteraction = false;
                _chart.View3D.XAxisPrimary3D.MouseDragSnapToDiv = false;
                _chart.View3D.XAxisPrimary3D.MouseScaling = false;
                _chart.View3D.XAxisPrimary3D.MouseScrolling = false;
            }

            //Y axis shows the depth
            {
                _chart.View3D.YAxisPrimary3D.SetRange(0, 200);
                _chart.View3D.YAxisPrimary3D.Title.Text = "Height";
                _chart.View3D.YAxisPrimary3D.LabelsColor = Colors.White;
                _chart.View3D.YAxisPrimary3D.Title.Color = Colors.LightCyan;
                _chart.View3D.YAxisPrimary3D.Units.Text = "m";
                _chart.View3D.YAxisPrimary3D.LabelTicksGap = 20;

                _chart.View3D.YAxisPrimary3D.MouseDragSnapToDiv = false;

                _chart.View3D.YAxisPrimary3D.MouseInteraction = false;
                _chart.View3D.YAxisPrimary3D.MouseDragSnapToDiv = false;
                _chart.View3D.YAxisPrimary3D.MouseScaling = false;
                _chart.View3D.YAxisPrimary3D.MouseScrolling = false;
            }

            //Z axis shows longitude
            {
                _chart.View3D.ZAxisPrimary3D.SetRange(m_mapCoordCorner.Longitude, m_mapCoordMaxLon.Longitude);

                _chart.View3D.ZAxisPrimary3D.ValueType = AxisValueType.MapCoordsDegNESW;

                _chart.View3D.ZAxisPrimary3D.Title.Text = "Longitude";
                _chart.View3D.ZAxisPrimary3D.LabelsColor = Colors.White;
                _chart.View3D.ZAxisPrimary3D.Title.Color = Colors.LightCyan;
                _chart.View3D.ZAxisPrimary3D.LabelTicksGap = 20;

                _chart.View3D.ZAxisPrimary3D.MouseDragSnapToDiv = false;

                _chart.View3D.ZAxisPrimary3D.MouseInteraction = false;
                _chart.View3D.ZAxisPrimary3D.MouseDragSnapToDiv = false;
                _chart.View3D.ZAxisPrimary3D.MouseScaling = false;
                _chart.View3D.ZAxisPrimary3D.MouseScrolling = false;
            }
        }

        private void InitCoordsLightsAxes()
        {
            InitMapCoords1M();

            InitLights();

            InitAxes();

            //Hide walls 
            List<WallBase> walls = _chart.View3D.GetWalls();
            foreach (WallBase wall in walls)
            {
                wall.Visible = false;
            }
        }


        List<SeriesPoint3D> lseriesPoint3Ds = new List<SeriesPoint3D>();

        List<SeriesPoint3D> lseries2Point3Ds = new List<SeriesPoint3D>();

        private void AxisVisibility(bool value)
        {
            _chart.View3D.XAxisPrimary3D.Visible = value;
            _chart.View3D.XAxisPrimary3D.LabelsVisible = value;
            _chart.View3D.YAxisPrimary3D.Visible = value;
            _chart.View3D.YAxisPrimary3D.LabelsVisible = value;
            _chart.View3D.ZAxisPrimary3D.Visible = value;
            _chart.View3D.ZAxisPrimary3D.LabelsVisible = value;
        }

        private void AddAnnotation()
        {
            Annotation3D targetValueLabel = new Annotation3D(_chart.View3D, Axis3DBinding.Primary, Axis3DBinding.Primary, Axis3DBinding.Primary);

            targetValueLabel.TargetCoordinateSystem = AnnotationTargetCoordinates.AxisValues;

            targetValueLabel.LocationCoordinateSystem = CoordinateSystem.RelativeCoordinatesToTarget;

            targetValueLabel.Visible = false;
            targetValueLabel.MouseInteraction = false;
            targetValueLabel.Style = AnnotationStyle.RoundedCallout;
            targetValueLabel.Shadow.Visible = false;
            _chart.View3D.Annotations.Add(targetValueLabel);
        }

        private void SetAnnotation(MeshModel meshModel)
        {
            _chart.View3D.Annotations[0].TargetAxisValues.SetValues(meshModel.Position.X, meshModel.Position.Y, meshModel.Position.Z);
            //_chart.View3D.Annotations[0].Text = "DJI" + "\nX: " + meshModel.Position.X.ToString("F6") + "\nY: " + meshModel.Position.Y.ToString("F0") + "\nZ: " + meshModel.Position.Z.ToString("F6");
            _chart.View3D.Annotations[0].Text = settings3D.DroneModel + "\nF = " + settings3D.DroneFreq + "\nH = " + meshModel.Position.Y.ToString("F0") + " m";
        }

        private void SetAnnotationPlus(MeshModel meshModel)
        {
            _chart.View3D.Annotations[0].TargetAxisValues.SetValues(meshModel.Position.X, meshModel.Position.Y, meshModel.Position.Z);
            _chart.View3D.Annotations[0].Text = settings3D.DroneModel + "\nF = " + settings3D.DroneFreq + "\nH = " + meshModel.Position.Y.ToString("F0") + " m";

            if (isCoordsInZone((meshModel.Position.X, meshModel.Position.Z), YellowZone))
            {
                _chart.View3D.Annotations[0].Fill.Color = System.Windows.Media.Color.FromArgb(255, 255, 255, 192);
                _chart.View3D.Annotations[0].Fill.GradientColor = System.Windows.Media.Color.FromArgb(255, 255, 128, 0);

                if (_chart.View3D.Annotations[0].Visible == false)
                    _chart.View3D.Annotations[0].Visible = true;

                if (isCoordsInZone((meshModel.Position.X, meshModel.Position.Z), RedZone))
                {
                    _chart.View3D.Annotations[0].Fill.Color = System.Windows.Media.Color.FromArgb(255, 255, 0, 0);
                    _chart.View3D.Annotations[0].Fill.GradientColor = System.Windows.Media.Color.FromArgb(255, 255, 0, 0);
                }
            }
            else
            {
                if (annotation.IsChecked.Value)
                {

                }
                else
                {
                    if (_chart.View3D.Annotations[0].Visible == true)
                        _chart.View3D.Annotations[0].Visible = false;
                }
            }
        }

        private void SetAnnotation(MeshModel meshModel, int ID)
        {
            _chart.View3D.Annotations[0].TargetAxisValues.SetValues(meshModel.Position.X, meshModel.Position.Y, meshModel.Position.Z);
            _chart.View3D.Annotations[0].Text = "DJI " + "ID: " + ID + "\nX: " + meshModel.Position.X.ToString("F6") + "\nY: " + meshModel.Position.Y.ToString("F0") + "\nZ: " + meshModel.Position.Z.ToString("F6");
        }

        private void DispatchIfNecessary(Action action)
        {
            if (!Dispatcher.CheckAccess())
                Dispatcher.Invoke(action);
            else
                action.Invoke();
        }

        private bool isDemo = false;
        private bool isDemoRequest = false;
        private void Demo()
        {
            isDemo = !isDemo;

            if (isDemo)
            {
                isDemoRequest = true;
                Task.Run(() => DemoRequest());
            }
            else
            {
                isDemoRequest = false;
            }
        }

        //Таск запроса
        private int demoCount = 0;
        private async void DemoRequest()
        {
            while (isDemoRequest)
            {
                if (demoCount == aeroScopes.Count)
                {
                    demoCount = 0;
                    lseriesPoint3Ds.Clear();
                    lseries2Point3Ds.Clear();
                }

                Plot(aeroScopes[demoCount].latitude, aeroScopes[demoCount].longitude, (float)aeroScopes[demoCount].altitude);

                int Ti = demoCount;
                int Ti1 = demoCount + 1;
                if (Ti1 == aeroScopes.Count) Ti1 = 0;

                int delay = Math.Abs(aeroScopes[Ti1].time - aeroScopes[Ti].time);
                if (delay > 10000) delay = 5000;
                delay = (int)Math.Abs((delay * settings3D.TimeCoef));

                await Task.Delay(delay);

                demoCount++;
            }
        }

        /// <summary>
        /// Plot
        /// </summary>
        /// <param name="x">latitude</param>
        /// <param name="z">longitude</param>
        /// <param name="y">altitude</param>
        private void Plot(float x, float z, float y)
        {
            DispatchIfNecessary(() =>
            {
                _chart.View3D.MeshModels[1].Position.X = x;
                _chart.View3D.MeshModels[1].Position.Y = y;
                _chart.View3D.MeshModels[1].Position.Z = z;

                //SetAnnotation(_chart.View3D.MeshModels[1]);
                SetAnnotationPlus(_chart.View3D.MeshModels[1]);

                if (lseriesPoint3Ds.Count < tailCount1)
                {
                    lseriesPoint3Ds.Add(new SeriesPoint3D(x, y, z));
                }
                else
                {
                    lseriesPoint3Ds.RemoveAt(0);
                    lseriesPoint3Ds.Add(new SeriesPoint3D(x, y, z));
                }

                if (lseries2Point3Ds.Count < tailCount2)
                {
                    lseries2Point3Ds.Add(new SeriesPoint3D(x, y, z));
                    lseries2Point3Ds.Add(new SeriesPoint3D(x, 0, z));
                    lseries2Point3Ds.Add(new SeriesPoint3D(x, y, z));
                }
                else
                {
                    lseries2Point3Ds.RemoveAt(0);
                    lseries2Point3Ds.RemoveAt(0);
                    lseries2Point3Ds.RemoveAt(0);
                    lseries2Point3Ds.Add(new SeriesPoint3D(x, y, z));
                    lseries2Point3Ds.Add(new SeriesPoint3D(x, 0, z));
                    lseries2Point3Ds.Add(new SeriesPoint3D(x, y, z));
                }

                dataSeries.Points = lseriesPoint3Ds.ToArray();

                dataSeries2.Points = lseries2Point3Ds.ToArray();

            });
        }

        private void VDrone_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _chart.View3D.MeshModels[1].Visible = vDrone.IsChecked.Value;
            }
            catch { }
        }

        private void LegendBox_Click(object sender, RoutedEventArgs e)
        {
            _chart.View3D.LegendBox.Visible = !_chart.View3D.LegendBox.Visible;
        }

        private void Annotation_Click(object sender, RoutedEventArgs e)
        {
            _chart.View3D.Annotations[0].Visible = !_chart.View3D.Annotations[0].Visible;
        }

        private void ToggleButton_Click(object sender, RoutedEventArgs e)
        {
            myGrid.ColumnDefinitions[1].Width = (tbVPE.IsChecked.Value) ? new GridLength(1, GridUnitType.Auto) : new GridLength(0);
        }

        private void Zones_Click(object sender, RoutedEventArgs e)
        {
            ZonesVisibility = !ZonesVisibility;
        }
    }
}
