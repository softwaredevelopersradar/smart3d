﻿using System;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using System.ComponentModel;

namespace smart3D
{
    public partial class TextBoxEx : UserControl
	{
		public delegate void ValueChangedDelegate(Object sender, string oldValue, string newValue);
		public delegate void ValueChangingDelegate();

		private DispatcherTimer _dispatcherTimer;
		private string _oldValue;

		[Browsable(true)]
		public event ValueChangedDelegate TextChanged;
		[Browsable(true)]
		public event ValueChangingDelegate TextChanging;

		public TextBoxEx()
		{
			InitializeComponent();

			_dispatcherTimer = new DispatcherTimer();
			_dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 10);
			_dispatcherTimer.Tick += new EventHandler(m_dispatcherTimer_Tick);

			_oldValue = "";

			TextChanged = null;
			TextChanging = null;
		}

		[Browsable(true)]
		public string Text
		{
			get
			{
				return textBox.Text;
			}

			set
			{
				textBox.Text = value;
			}
		}

		[Browsable(true)]
		public int IntervalInMs
		{
			get
			{
				return _dispatcherTimer.Interval.Milliseconds;
			}

			set
			{
				_dispatcherTimer.Stop();
				_dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, value);
			}
		}

		private void m_dispatcherTimer_Tick(object sender, EventArgs e)
		{
			HandleTextChanging();
		}

		private void textBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			_dispatcherTimer.Stop();

			if (TextChanging != null)
			{
				TextChanging();
			}

			if (TextChanged != null)
			{
				_dispatcherTimer.Start();
			}
		}

		private void HandleTextChanging()
		{
			_dispatcherTimer.Stop();

			if (TextChanged != null)
			{
				TextChanged(this, _oldValue, textBox.Text);

				_oldValue = textBox.Text;
			}
		}

		private void textBox_KeyDown(object sender, KeyEventArgs e)
		{
			if ((e.Key == Key.Enter) || (e.Key == Key.Return))
			{
				HandleTextChanging();	
			}
		}
	}
}
