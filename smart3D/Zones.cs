﻿using Arction.Wpf.SemibindableCharting;
using Arction.Wpf.SemibindableCharting.Annotations;
using Arction.Wpf.SemibindableCharting.Series3D;
using Arction.Wpf.SemibindableCharting.Views.View3D;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace smart3D
{
    public partial class MainWindow : Window
    {
        private double calculateTheDistance(double latA, double lonA, double latB, double lonB)
        {
            double EARTH_RADIUS = 6372795;

            /*
            * Расстояние между двумя точками
            * $φA, $λA - широта, долгота 1-й точки,
            * $φB, $λB - широта, долгота 2-й точки
            *
            */

            // перевести координаты в радианы
            var lat1 = latA * Math.PI / 180;
            var lat2 = latB * Math.PI / 180;
            var long1 = lonA * Math.PI / 180;
            var long2 = lonB * Math.PI / 180;

            // косинусы и синусы широт и разницы долгот
            var cl1 = Math.Cos(lat1);
            var cl2 = Math.Cos(lat2);
            var sl1 = Math.Sin(lat1);
            var sl2 = Math.Sin(lat2);
            var delta = long2 - long1;
            var cdelta = Math.Cos(delta);
            var sdelta = Math.Sin(delta);

            // вычисления длины большого круга
            var y = Math.Sqrt(Math.Pow(cl2 * sdelta, 2) + Math.Pow(cl1 * sl2 - sl1 * cl2 * cdelta, 2));
            var x = sl1 * sl2 + cl1 * cl2 * cdelta;

            //
            var ad = Math.Atan2(y, x);
            var dist = ad * EARTH_RADIUS;

            return dist;
        }

        public Zone AddZone(System.Windows.Media.Color color, (double Latitude, double Longitude) CenterCoord, double RadiusX, double RadiusZ, double inc = 1.0d, double recWidth = 1.0, double recHeight = 30.0d)
        {
            List<Rectangle3D> rectangle3Ds = new List<Rectangle3D>();

            var xstart = _chart.View3D.XAxisPrimary3D.Minimum;
            var zstart = _chart.View3D.ZAxisPrimary3D.Minimum;

            var xwidth = (float)(_chart.View3D.XAxisPrimary3D.Maximum - _chart.View3D.XAxisPrimary3D.Minimum);
            var zwidth = (float)(_chart.View3D.ZAxisPrimary3D.Maximum - _chart.View3D.ZAxisPrimary3D.Minimum);

            var d1 = calculateTheDistance(m_mapCoordCorner.Latitude, m_mapCoordCorner.Longitude, m_mapCoordMaxLat.Latitude, m_mapCoordMaxLat.Longitude);
            var d2 = calculateTheDistance(m_mapCoordCorner.Latitude, m_mapCoordCorner.Longitude, m_mapCoordMaxLon.Latitude, m_mapCoordMaxLon.Longitude);

            double coefX = (RadiusX * xwidth) / d1;
            double coefZ = (RadiusZ * zwidth) / d2;

            for (double point = 0; point < 360d; point += inc)
            {
                // Rotated small rectangle.
                Rectangle3D r3 = new Rectangle3D(_chart.View3D, Axis3DBinding.Primary, Axis3DBinding.Primary, Axis3DBinding.Primary);

                r3.Size.Width = 1.0;
                r3.Size.Height = recHeight;

                double x = Math.Cos(2 * Math.PI * point / 360) * coefX + CenterCoord.Latitude;
                double z = Math.Sin(2 * Math.PI * point / 360) * coefZ + CenterCoord.Longitude;

                r3.Center.SetValues(x, r3.Size.Height * 2, z);

                r3.Fill.UseImage = false;

                r3.Fill.Material.DiffuseColor = color;

                r3.Rotation.SetValues(90, 0, point);

                r3.MouseHighlight = MouseOverHighlight.None;

                r3.MouseInteraction = false;

                r3.Visible = _ZonesVisibility;

                rectangle3Ds.Add(r3);

            }

            _chart.View3D.Rectangles.AddRange(rectangle3Ds);

            return new Zone(CenterCoord, RadiusX, RadiusZ);
        }

        private bool _ZonesVisibility = false;
        public bool ZonesVisibility
        {
            get { return _ZonesVisibility; }
            set
            {
                if (_ZonesVisibility != value)
                {
                    _ZonesVisibility = value;
                    if (_chart.View3D.Rectangles.Count > 0)
                    {
                        for (int i = 0; i < _chart.View3D.Rectangles.Count; i++)
                        {
                            _chart.View3D.Rectangles[i].Visible = _ZonesVisibility;
                        }
                    }
                }
            }
        }

        public void ClearZones()
        {
            _chart.View3D.Rectangles.Clear();
        }

        public bool isCoordsInZone((double Latitude, double Longitude) Coords, Zone zone)
        {
            var d = calculateTheDistance(Coords.Latitude, Coords.Longitude, zone.CenterCoord.Latitude, zone.CenterCoord.Longitude);

            if (d <= zone.RadiusX && d <= zone.RadiusZ)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }

    public class Zone
    {
        public (double Latitude, double Longitude) CenterCoord;
        public double RadiusX;
        public double RadiusZ;

        public Zone((double Latitude, double Longitude) centerCoord, double radiusX, double radiusZ)
        {
            CenterCoord = centerCoord;
            RadiusX = radiusX;
            RadiusZ = radiusZ;
        }
    }

}
