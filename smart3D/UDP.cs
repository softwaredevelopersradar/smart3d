﻿using Arction.Wpf.SemibindableCharting;
using System;
using System.Net;
using System.Windows;

namespace smart3D
{
    public partial class MainWindow
    {
        UDPReceiver.UDPReceiver uDPReceiver;

        private void InitUDPReceiver(Settings3D settings3D)
        {
            //TryParse
            var myIp = IPAddress.Parse(settings3D.UDPmyIP);
            var myport = settings3D.UDPmyPort;
            var remoteIp = IPAddress.Parse(settings3D.UDPremoteIP);
            var remoteport = settings3D.UDPremotePort;
            byte addrSender = 0;
            byte addrRecipient = 0;

            uDPReceiver = new UDPReceiver.UDPReceiver(myIp, myport, remoteIp, remoteport, addrSender, addrRecipient);

            //uDPReceiver.OnGetCoordRDM += UDPReceiver_OnGetCoordRDM;
            uDPReceiver.OnGetCoordAero += UDPReceiver_OnGetCoordAero;

            uDPReceiver.OnConnect += UDPReceiver_OnConnect;
            uDPReceiver.OnDisconnect += UDPReceiver_OnDisconnect;

            uDPReceiver.Connect();
        }

        private void UDPReceiver_OnConnect(object sender, bool e)
        {
            //UPD.Background = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Green);
        }
        private void UDPReceiver_OnDisconnect(object sender, bool e)
        {
            //UPD.Background = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Red);
        }

        private void UDPReceiver_OnGetCoordAero(object sender, UDPReceiver.Coord e)
        {
            Plot((float)e.latitude, (float)e.longitude, (float)e.altitude);
        }
    }
}
