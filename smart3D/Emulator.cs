﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace smart3D
{
    public partial class MainWindow
    {

        List<AeroScopeEmulator.Emulator.AeroScope> aeroScopes = new List<AeroScopeEmulator.Emulator.AeroScope>();

        private void LoadAeroScopeEmulator(string name)
        {
            string temp = System.Reflection.Assembly.GetEntryAssembly().Location;
            var lastindex = temp.LastIndexOf("\\");
            temp = temp.Substring(0, lastindex + 1);
            temp = temp + name + ".txt";

            aeroScopes = AeroScopeEmulator.Emulator.GetSamples(temp);
        }

    }
}
